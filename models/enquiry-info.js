const mongoose = require("mongoose");
const constants = require("../config/constants.json");

const enquiryInfoSchema = mongoose.Schema(
  {
    phone: { type: String, required: true },
    email: { type: String, required: true },
    type: {
      type: String,
      required: true,
      enum: [constants.ENQUIRY_TYPE.ADMISSION, constants.ENQUIRY_TYPE.GENERAL],
    },
  },
  { timestamps: true }
);

const enquiryInfoModel = mongoose.model("enquiry-info", enquiryInfoSchema);

module.exports = enquiryInfoModel;
