const mongoose = require("mongoose");
const constants = require("../config/constants.json");

const enquirySchema = mongoose.Schema(
  {
    phoneNumber: { type: String, required: true },
    email: { type: String, required: true },
    name: { type: String, required: true },
    type: {
      type: String,
      required: true,
      enum: [constants.ENQUIRY_TYPE.ADMISSION, constants.ENQUIRY_TYPE.GENERAL],
    },
    program: { type: String, required: true },
    enquiry: { type: String, required: true },
  },
  { timestamps: true }
);

const enquiryModel = mongoose.model("enquiry", enquirySchema);

module.exports = enquiryModel;
