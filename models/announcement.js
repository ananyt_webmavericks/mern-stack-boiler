const mongoose = require("mongoose");
const constants = require("../config/constants.json");

const announcementSchema = mongoose.Schema(
  {
    title: { type: String, required: true },
    description: { type: String },
    status: {
      type: String,
      required: true,
      default: constants.STATUS.ACTIVE,
      enum: [constants.STATUS.ACTIVE, constants.STATUS.INACTIVE],
    },
    link: { type: String },
    type: {
      type: String,
      required: true,
      enum: [constants.ANNOUNCEMENT.ANNOUNCEMENT, constants.ANNOUNCEMENT.EVENT],
    },
  },
  { timestamps: true }
);

const Announcement = mongoose.model("announcement", announcementSchema);

module.exports = Announcement;
