const mongoose = require("mongoose");
const constants = require("../config/constants.json");

const isValidName = async (name) => {
  const specialCharacters = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
  if (specialCharacters.test(name)) {
    return false;
  }
  return true;
};

const isValidEmail = async (email) => {
  const emailRegEx =
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (!emailRegEx.test(email)) {
    return false;
  }
  return true;
};

const superAdminSchema = mongoose.Schema(
  {
    name: { type: String, required: true, validate: isValidName },
    email: { type: String, required: true, validate: isValidEmail },
    password: { type: String, required: true },
    passwordChanged: {
      type: Boolean,
      default: constants.STATUS.INACTIVE,
      enum: [constants.STATUS.INACTIVE, constants.STATUS.ACTIVE],
    },
    verified: {
      type: Boolean,
      default: constants.STATUS.INACTIVE,
      enum: [constants.STATUS.INACTIVE, constants.STATUS.ACTIVE],
    },
    status: {
      type: Boolean,
      default: constants.STATUS.INACTIVE,
      enum: [constants.STATUS.INACTIVE, constants.STATUS.ACTIVE],
    },
  },
  { timestamps: true }
);

superAdminSchema.index({ email: 1 }, { unique: true, name: "IDX_ADMIN_EMAIL" });
const superAdminModel = mongoose.model("admin", superAdminSchema);

module.exports = superAdminModel;
