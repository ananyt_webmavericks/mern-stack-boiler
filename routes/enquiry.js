const express = require("express");
const router = express.Router();
const enquiryController = require("../controllers/enquiry");
const checkJWT = require("../middlewares/check-jwt");
const enquiryMiddleware = require("../middlewares/enquiry");

router.post(
  "/create",
  enquiryMiddleware.validateEnquiryForm,
  enquiryController.createEnquiry
);

router.get(
  "/list-all-admissions-enquiry",
  checkJWT.validateAdmin,
  enquiryController.getAllAdmissionEnquiry
);

router.get(
  "/list-all-general-enquiry",
  checkJWT.validateAdmin,
  enquiryController.getAllGeneralEnquiry
);

router.post(
  "/create-enquiry-info",
  checkJWT.validateAdmin,
  enquiryMiddleware.validateEnquiryInfo,
  enquiryController.createEnquryInfo
);

router.get(
  "/get-admission-enquiry-info",
  enquiryController.getAdmissionEnquiryInfo
);
router.get(
  "/get-general-enquiry-info",
  enquiryController.getGeneralEnquiryInfo
);

router.patch(
  "/update-enquiry-info",
  checkJWT.validateAdmin,
  enquiryMiddleware.validateEnquiryInfo,
  enquiryController.updateEnquiryInfo
);

module.exports = router;
