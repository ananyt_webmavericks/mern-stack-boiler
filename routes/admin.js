const express = require("express");
const router = express.Router();
const adminController = require("../controllers/admin");
const checkJWT = require("../middlewares/check-jwt");
const adminMiddleware = require("../middlewares/admin");

router.post(
  "/create-admin",
  checkJWT.validateAdmin,
  adminMiddleware.createAdmin,
  adminController.createAdmin
);
router.post("/login", adminMiddleware.validateLogin, adminController.login);
router.post(
  "/change-password",
  checkJWT.validateAdmin,
  adminMiddleware.validateChangePassword,
  adminController.changePassword
);

router.patch(
  "/change-admin-status",
  checkJWT.validateAdmin,
  adminMiddleware.validateChangeAdminStatus,
  adminController.changeAdminStatus
);

router.get(
  "/get-all-admins",
  checkJWT.validateAdmin,
  adminController.getAllAdmins
);

module.exports = router;
