const express = require("express");
const router = express.Router();
const checkJWT = require("../middlewares/check-jwt");
const announcementMiddleware = require("../middlewares/announcement");
const announcementContoller = require("../controllers/announcement");

router.post(
  "/create",
  checkJWT.validateAdmin,
  announcementMiddleware.validateCreateAnnouncement,
  announcementContoller.createAnnouncement
);

router.patch(
  "/change-announcement-status",
  checkJWT.validateAdmin,
  announcementMiddleware.validateChangeAnnouncementStatus,
  announcementContoller.changeAnnouncementStatus
);

router.get("/get-all-announcements", announcementContoller.getAllAnnouncements);
router.get("/get-all-events", announcementContoller.getAllEvents);

router.delete(
  "/delete-announcement/:id",
  checkJWT.validateAdmin,
  announcementContoller.deleteAnnouncement
);
module.exports = router;
