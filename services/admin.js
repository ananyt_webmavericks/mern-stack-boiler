const { sendMail } = require("../utility/send-mail");
const { generatePassword } = require("../utility/generate-password");
const superAdminModel = require("../models/admin");
const constants = require("../config/constants.json");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { db } = require("../models/admin");

const createAdmin = async (createAdminObject) => {
  try {
    const { name, email } = createAdminObject;

    let data = {
      name: name,
      email: email,
      status: constants.STATUS.INACTIVE,
      password: await generatePassword(),
    };
    let newUser = new superAdminModel(data);
    let createUser = await newUser.save(data);
    if (!createUser) {
      return {
        status: false,
        message: "Something went wrong!!",
      };
    }
    sendMail(email, data.password);
    return {
      status: true,
      user: createUser,
    };
  } catch (error) {
    throw error;
  }
};

const login = async (email, password) => {
  try {
    let getUser = await superAdminModel.findOne({
      email: email,
      passoword: password,
      status: constants.STATUS.ACTIVE,
    });
    if (!getUser) {
      return {
        status: false,
        message: "User not found!",
      };
    }

    if (getUser.passwordChanged) {
      if (await bcrypt.compare(password, getUser.password)) {
        const token = jwt.sign(
          { user_id: getUser._id, email: email, userType: "SUPER-ADMIN" },
          process.env.TOKEN_KEY,
          {
            expiresIn: "2h",
          }
        );
        return {
          status: true,
          token: token,
        };
      } else {
        return {
          status: false,
          message: "Incorrect Credentials !!",
        };
      }
    } else {
      if (password === getUser.password) {
        const token = jwt.sign(
          { user_id: getUser._id, email: email, userType: "SUPER-ADMIN" },
          process.env.TOKEN_KEY,
          {
            expiresIn: "2h",
          }
        );

        return {
          status: true,
          token: token,
        };
      } else {
        return {
          status: false,
          message: "Invalid Credentials!",
        };
      }
    }
  } catch (error) {
    throw error;
  }
};

const changePassword = async (oldPassword, newPassword, user) => {
  try {
    const dbUser = await superAdminModel.findOne({ email: user.email });
    if (!dbUser) {
      return {
        status: false,
        message: "User doesn't exist",
      };
    }
    if (!(dbUser.passwordChanged && dbUser.verified)) {
      const getNewUser = await superAdminModel.findOne({
        email: user.email,
        password: oldPassword,
      });
      if (!getNewUser) {
        return {
          status: false,
          message: "OldPassword doesn't match",
        };
      }
      getNewUser.passwordChanged = constants.STATUS.ACTIVE;
      getNewUser.verified = constants.STATUS.ACTIVE;
      getNewUser.password = await bcrypt.hash(newPassword, 10);
      const updatedUser = await getNewUser.save();
      return {
        status: true,
        updatedUser: updatedUser,
      };
    } else {
      const checkOldPassword = await bcrypt.compare(
        oldPassword,
        dbUser.password
      );
      if (!checkOldPassword) {
        return {
          status: false,
          message: "Oldpassword doesn't match!",
        };
      }

      dbUser.password = await bcrypt.hash(newPassword, 10);

      const updatedUser = await dbUser.save();
      return {
        status: true,
        updatedUser: updatedUser,
      };
    }
  } catch (error) {
    throw error;
  }
};

const changeAdminStatus = async (adminId, status) => {
  try {
    const getUser = await superAdminModel.findOne({ _id: adminId });
    if (!getUser) {
      return {
        status: false,
        message: "Admin not found!",
      };
    }
    getUser.status = status;
    const updatedUser = await getUser.save();
    return {
      status: true,
      message: updatedUser,
    };
  } catch (error) {
    throw error;
  }
};

const getAllAdmins = async () => {
  try {
    return superAdminModel.find({}, { name: 1, email: 1, status: 1 });
  } catch (error) {
    throw error;
  }
};

module.exports = {
  createAdmin,
  login,
  changePassword,
  changeAdminStatus,
  getAllAdmins,
};
