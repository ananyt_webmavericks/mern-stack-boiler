const Announcement = require("../models/announcement");
const constants = require("../config/constants.json");

const createAnnouncement = async (createObject) => {
  try {
    const announcement = new Announcement(createObject);
    return announcement.save();
  } catch (error) {
    throw error;
  }
};

const changeAnnouncementStatus = async (status, announcementId) => {
  try {
    const getAnnouncement = await Announcement.findOne({ _id: announcementId });
    if (!getAnnouncement) {
      return {
        status: false,
        message: "Announcement not found!!",
      };
    }

    getAnnouncement.status = status;

    const updateAnnouncement = await getAnnouncement.save();

    return {
      status: true,
      announcement: updateAnnouncement,
    };
  } catch (error) {
    throw error;
  }
};

const getAllAnnouncements = async (data) => {
  try {
    return Announcement.find(data);
  } catch (error) {
    throw error;
  }
};

const deleteAnnouncement = async (id) => {
  try {
    return Announcement.deleteOne({ _id: id });
  } catch (error) {
    throw error;
  }
};

module.exports = {
  createAnnouncement,
  changeAnnouncementStatus,
  getAllAnnouncements,
  deleteAnnouncement,
};
