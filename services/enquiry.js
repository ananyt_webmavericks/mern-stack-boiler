const enquiryModel = require("../models/enquiry");
const enquiryInfoModel = require("../models/enquiry-info");

const createEnquiry = async (createObject) => {
  try {
    const enquiry = new enquiryModel(createObject);

    const newEnquiry = await enquiry.save();

    return {
      status: true,
      enquiry: newEnquiry,
    };
  } catch (error) {
    throw error;
  }
};

const getAllEnquiry = async (filter) => {
  try {
    return enquiryModel.find(filter);
  } catch (error) {
    throw error;
  }
};

const createEnquryInfo = async (data) => {
  try {
    const enquiryInfo = new enquiryInfoModel(data);

    return await enquiryInfo.save();
  } catch (error) {
    throw error;
  }
};

const getEnquiryInfo = async (filter) => {
  try {
    return await enquiryInfoModel.findOne(filter, {
      phone: 1,
      email: 1,
      _id: 0,
    });
  } catch (error) {
    throw error;
  }
};

const updateEnquiryInfo = async (updateObject) => {

    try {

        const getEnquiryInfo = await enquiryInfoModel.findOne({type:updateObject.type});
        if(!getEnquiryInfo) {
            return {
                status:false,
                message:"Info can not be updated"
            }
        }

        getEnquiryInfo.phone = updateObject.phone
        getEnquiryInfo.email = updateObject.email
        const updatedEnquiryInfo = await getEnquiryInfo.save()
        return {
            status:true,
            updatedInfo:updatedEnquiryInfo
        }

    }catch(error) {

    }

}

module.exports = {
  createEnquiry,
  getAllEnquiry,
  createEnquryInfo,
  getEnquiryInfo,
  updateEnquiryInfo
};
