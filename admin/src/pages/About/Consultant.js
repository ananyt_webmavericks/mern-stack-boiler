import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { About } from '../../service/About/About';

const Consultant = () => {


    let emptyconsult = {
        consultantId: null,
        id: null,
        consultantname: '',
        consultanttitle: '',
    };

    const [consultant, setConsultant] = useState(null);
    const [concDialog, setConcDialog] = useState(false);
    const [deleteConcDialog, setDeleteConcDialog] = useState(false);
    const [deleteConsultantDialog, setDeleteConsultantDialog] = useState(false);
    const [consult, setConsult] = useState(emptyconsult);
    const [selectedConsultant, setSelectedConsultant] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const about = new About();
        about.getConsultant().then(data => setConsultant(data));
    }, []);

    const openNew = () => {
        setConsult(emptyconsult);
        setSubmitted(false);
        setConcDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setConcDialog(false);
    }

    const hidedeleteConcDialog = () => {
        setDeleteConcDialog(false);
    }

    const hidedeleteConsultantDialog = () => {
        setDeleteConsultantDialog(false);
    }

    const saveconsult = () => {
        setSubmitted(true);

        if (consult.name.trim()) {
            let _consultant = [...consultant];
            let _consult = { ...consult };
            if (consult.id) {
                const index = findIndexById(consult.id);

                _consultant[index] = _consult;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'consult Updated', life: 3000 });
            }
            else {
                _consult.id = createId();
                _consult.image = 'consult-placeholder.svg';
                _consultant.push(_consult);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'consult Created', life: 3000 });
            }

            setConsultant(_consultant);
            setConcDialog(false);
            setConsult(emptyconsult);
        }
    }

    const editconsult = (consult) => {
        setConsult({ ...consult });
        setConcDialog(true);
    }

    const confirmDeleteconsult = (consult) => {
        setConsult(consult);
        setDeleteConcDialog(true);
    }

    const deleteconsult = () => {
        let _consultant = consultant.filter(val => val.id !== consult.id);
        setConsultant(_consultant);
        setDeleteConcDialog(false);
        setConsult(emptyconsult);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'consult Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < consultant.length; i++) {
            if (consultant[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteConsultantDialog(true);
    }

    const deleteselectedConsultant = () => {
        let _consultant = consultant.filter(val => !selectedConsultant.includes(val));
        setConsultant(_consultant);
        setDeleteConsultantDialog(false);
        setSelectedConsultant(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'consultant Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _consult = { ...consult };
        _consult[`${name}`] = val;
        setConsult(_consult);
    }
    const onUploadChange = (e, image) => {
        const val = (e.target && e.target.value) || '';
        let _consult = { ...consult };
        _consult[`${image}`] = val;
        setConsult(_consult);
    }


    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedConsultant || !selectedConsultant.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>
                <FileUpload mode="basic" accept="image/*" maxFileSize={1000000} label="Import" chooseLabel="Import" className="mr-2 inline-block" />
                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const idBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }

    // const CategoryBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Category</span>
    //             {rowData.afsheading}
    //         </>
    //     );
    // }
  

    // const imageBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Image</span>
    //             <img src={`assets/demo/images/staff/${rowData.image}`} alt={rowData.image} className="shadow-2" width="100" />
    //         </>
    //     )
    // }
    const nameBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Department</span>
                {rowData.consultantname}
            </>
        );
    }


    const titleBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">title</span>
                {rowData.consultanttitle}
            </>
        );
    }

    // const positionBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Position</span>
    //           {rowData.afspostion}
    //         </>
    //     );
    // }


    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => editconsult(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeleteconsult(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">Consultant Staff</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const concDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={saveconsult} />
        </>
    );
    const deleteConcDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteConcDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteconsult} />
        </>
    );
    const deleteConsultantDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteConsultantDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedConsultant} />
        </>
    );

    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={consultant} selection={selectedConsultant} onSelectionChange={(e) => setSelectedConsultant(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} consultant"
                        globalFilter={globalFilter} emptyMessage="No consultant found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem'}}></Column>
                        <Column field="consultistration Id" header="consultistration Id" sortable body={idBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column>
                        {/* <Column field="Category" header="Category" sortable body={CategoryBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column> */}
                        <Column field="Name" header="Name" body={nameBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column>
                        <Column field="Title" header="Title" body={titleBodyTemplate} sortable headerStyle={{ width: '40%', minWidth: '8rem' }}></Column>
                        {/* <Column field="Position" header="Position" sortable body={positionBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column> */}
                       
                        {/* <Column field="inventoryStatus" header="Status" body={statusBodyTemplate} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column> */}
                        <Column header="Action" body={actionBodyTemplate}></Column>
                    </DataTable>

                    <Dialog visible={concDialog} style={{ width: '450px' }} header="Technical Staff" modal className="p-fluid" footer={concDialogFooter} onHide={hideDialog}>
                        
                        <div className="field">
                        {consult.image && <img src={`assets/demo/images/staff/${consult.image}`} alt={consult.image} width="150" className="mt-0 mx-auto mb-5 block shadow-2" />}
                        <FileUpload mode="basic" onChange={(e) => onUploadChange(e, 'image')} accept="image/*" maxFileSize={1000000} label="Upload Image" chooseLabel="Upload Image" className="mr-2 inline-block"/>
                        </div>
                        <div className="field">
                            <label htmlFor="name">Name</label>
                            <InputText id="name" value={consult.name} onChange={(e) => onInputChange(e, 'name')} required autoFocus className={classNames({ 'p-invalid': submitted && !consult.name })} />
                            {submitted && !consult.name && <small className="p-invalid">Name is required.</small>}
                        </div>
                        <div className="field">
                            <label htmlFor="title">Title</label>
                            <InputText id="title" value={consult.title} onChange={(e) => onInputChange(e, 'title')} required autoFocus className={classNames({ 'p-invalid': submitted && !consult.title })} />
                            {submitted && !consult.title && <small className="p-invalid">Title is required.</small>}
                        </div>
                        <div className="field">
                            <label htmlFor="department">Department</label>
                            <InputText id="department" value={consult.dpmt} onChange={(e) => onInputChange(e, 'department')} required autoFocus className={classNames({ 'p-invalid': submitted && !consult.dpmt })} />
                            {submitted && !consult.dpmt && <small className="p-invalid">Department is required.</small>}
                        </div>

                    </Dialog>

                    <Dialog visible={deleteConcDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteConcDialogFooter} onHide={hidedeleteConcDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {consult && <span>Are you sure you want to delete <b>{consult.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteConsultantDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteConsultantDialogFooter} onHide={hidedeleteConsultantDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {consult && <span>Are you sure you want to delete the selected consultant?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(Consultant, comparisonFn);