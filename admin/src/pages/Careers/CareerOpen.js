import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { InputTextarea } from 'primereact/inputtextarea';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Openings } from '../../service/Careers/Openings';



const CareerOpen = () => {


    let emptyCareerList = {
        id: null,
        careerHead: '',
        careerLink: '',
    };

    const [career, setCareer] = useState(null);
    const [openingsDialog, setOpeningsDialog] = useState(false);
    const [deleteOpeningsDialog, setDeleteOpeningsDialog] = useState(false);
    const [deleteCareerDialog, setDeleteCareerDialog] = useState(false);
    const [careerList, setCareerList] = useState(emptyCareerList);
    const [selectedCareer, setSelectedCareer] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const openings = new Openings();
        openings.getCareeropen().then(data => setCareer(data));
    }, []);

    const formatCurrency = (value) => {
        return value.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
    }

    const openNew = () => {
        setCareerList(emptyCareerList);
        setSubmitted(false);
        setOpeningsDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setOpeningsDialog(false);
    }

    const hidedeleteOpeningsDialog = () => {
        setDeleteOpeningsDialog(false);
    }

    const hidedeleteCareerDialog = () => {
        setDeleteCareerDialog(false);
    }

    const savecareerList = () => {
        setSubmitted(true);

        if (careerList.name.trim()) {
            let _career = [...career];
            let _careerList = { ...careerList };
            if (careerList.id) {
                const index = findIndexById(careerList.id);

                _career[index] = _careerList;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'careerList Updated', life: 3000 });
            }
            else {
                _careerList.id = createId();
                _careerList.image = 'careerList-placeholder.svg';
                _career.push(_careerList);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'careerList Created', life: 3000 });
            }

            setCareer(_career);
            setOpeningsDialog(false);
            setCareerList(emptyCareerList);
        }
    }

    const editcareerList = (careerList) => {
        setCareerList({ ...careerList });
        setOpeningsDialog(true);
    }

    const confirmDeletecareerList = (careerList) => {
        setCareerList(careerList);
        setDeleteOpeningsDialog(true);
    }

    const deletecareerList = () => {
        let _career = career.filter(val => val.id !== careerList.id);
        setCareer(_career);
        setDeleteOpeningsDialog(false);
        setCareerList(emptyCareerList);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'careerList Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < career.length; i++) {
            if (career[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteCareerDialog(true);
    }

    const deleteselectedCareer = () => {
        let _career = career.filter(val => !selectedCareer.includes(val));
        setCareer(_career);
        setDeleteCareerDialog(false);
        setSelectedCareer(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'career Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _careerList = { ...careerList };
        _careerList[`${name}`] = val;
        setCareerList(_careerList);
    }



    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedCareer || !selectedCareer.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>

                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const codeBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }

    const nameBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Announcements</span>
                {rowData.careerHead}
            </>
        );
    }

    // const imageBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">LinkTo</span>
    //             <img src={`assets/demo/images/staff/${rowData.image}`} alt={rowData.image} className="shadow-2" width="100" />
    //         </>
    //     )
    // }

    const priceBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">LinkTo</span>
                {formatCurrency(rowData.careerLink)}
            </>
        );
    }

    // const categoryBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Department</span>
    //             {rowData.dpmt}
    //         </>
    //     );
    // }

    // const ratingBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Readmore</span>
    //             <Rating value={rowData.readmore} readonly cancel={false} />
    //         </>
    //     );
    // }


    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => editcareerList(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeletecareerList(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">Career Openings</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const openingsDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={savecareerList} />
        </>
    );
    const deleteOpeningsDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteOpeningsDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deletecareerList} />
        </>
    );
    const deleteCareerDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteCareerDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedCareer} />
        </>
    );



    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={career} selection={selectedCareer} onSelectionChange={(e) => setSelectedCareer(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} career"
                        globalFilter={globalFilter} emptyMessage="No career found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                        <Column field="Id" header="Id" sortable body={codeBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column field="career Openings" header="career cal" sortable body={nameBodyTemplate} headerStyle={{ width: '40%', minWidth: '10rem' }}></Column>
                        <Column field="Pdf" header="Pdf" body={priceBodyTemplate} style={{ width: "40%" }} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        {/* <Column field="inventoryStatus" header="Status" body={statusBodyTemplate} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column> */}
                        <Column header="Action" body={actionBodyTemplate} style={{ float: "right",  paddingBottom: "22px" ,width: "100%" }}></Column>
                    </DataTable>

                    <Dialog visible={openingsDialog} style={{ width: '450px' }} header="Technical Staff" modal className="p-fluid" footer={openingsDialogFooter} onHide={hideDialog}>
                    <div className="field">
                    <FileUpload  mode = "basic" accept="pdf/*"  label="Upload File" chooseLabel="Upload File" className="mr-2 inline-block"/>
 
                    </div>
                        <div className="field">
                            <label htmlFor="name">Announcement</label>
                            <InputTextarea id="announcement" value={careerList.announcement} onChange={(e) => onInputChange(e, 'name')} required autoFocus className={classNames({ 'p-invalid': submitted && !careerList.announcement })} />
                            {submitted && !careerList.name && <small className="p-invalid">Announcement is required.</small>}
                        </div>
                        <div className="field">
                            <label htmlFor="title">link To</label>
                            <InputText id="linkTo" value={careerList.linkTo} onChange={(e) => onInputChange(e, 'title')} required autoFocus className={classNames({ 'p-invalid': submitted && !careerList.linkTo })} />

                        </div>

                    </Dialog>

                    <Dialog visible={deleteOpeningsDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteOpeningsDialogFooter} onHide={hidedeleteOpeningsDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {careerList && <span>Are you sure you want to delete <b>{careerList.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteCareerDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteCareerDialogFooter} onHide={hidedeleteCareerDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {careerList && <span>Are you sure you want to delete the selected career?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(CareerOpen, comparisonFn);