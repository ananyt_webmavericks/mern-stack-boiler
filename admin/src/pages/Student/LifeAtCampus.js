import React, { useState, useEffect, useRef } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Student } from '../../service/Student/Student';



const LifeAtCampus = () => {

    const chooseOptions = { label: 'Choose', icon: 'pi pi-fw pi-plus' };
    const uploadOptions = { label: 'Uplaod', icon: 'pi pi-upload', className: 'p-button-success' };
    const cancelOptions = { label: 'Cancel', icon: 'pi pi-times', className: 'p-button-danger' };
    let emptyEvents = {
        id: null,
        lifecampusstuid: null,
        lifecampusstuimg: '',
    };

    const [studentImg, setStudentImg] = useState(null);
    const [studentDialog, setStudentDialog] = useState(false);
    const [deleteStudentDialog, setDeleteStudentDialog] = useState(false);
    const [deleteStuImageDialog, setDeleteStuImageDialog] = useState(false);
    const [stuImage, setStuImage] = useState(emptyEvents);
    const [selectedStudentImg, setSelectedStudentImg] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const student = new Student();
        student.getLifeAtJaypee().then(data => setStudentImg(data));
    }, []);


    const openNew = () => {
        setStuImage(emptyEvents);
        setSubmitted(false);
        setStudentDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setStudentDialog(false);
    }

    const hidedeleteStudentDialog = () => {
        setDeleteStudentDialog(false);
    }

    const hidedeleteStuImageDialog = () => {
        setDeleteStuImageDialog(false);
    }

    const savestuImage = () => {
        setSubmitted(true);

        if (stuImage.name.trim()) {
            let _studentImg = [...studentImg];
            let _stuImage = { ...stuImage };
            if (stuImage.id) {
                const index = findIndexById(stuImage.id);

                _studentImg[index] = _stuImage;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'stuImage Updated', life: 3000 });
            }
            else {
                _stuImage.id = createId();
                _stuImage.image = 'stuImage-placeholder.svg';
                _studentImg.push(_stuImage);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'stuImage Created', life: 3000 });
            }

            setStudentImg(_studentImg);
            setStudentDialog(false);
            setStuImage(emptyEvents);
        }
    }

    const editstuImage = (stuImage) => {
        setStuImage({ ...stuImage });
        setStudentDialog(true);
    }

    const confirmDeletestuImage = (stuImage) => {
        setStuImage(stuImage);
        setDeleteStudentDialog(true);
    }

    const deletestuImage = () => {
        let _studentImg = studentImg.filter(val => val.id !== stuImage.id);
        setStudentImg(_studentImg);
        setDeleteStudentDialog(false);
        setStuImage(emptyEvents);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'stuImage Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < studentImg.length; i++) {
            if (studentImg[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteStuImageDialog(true);
    }

    const deleteselectedStudentImg = () => {
        let _studentImg = studentImg.filter(val => !selectedStudentImg.includes(val));
        setStudentImg(_studentImg);
        setDeleteStuImageDialog(false);
        setSelectedStudentImg(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'studentImg Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _stuImage = { ...stuImage };
        _stuImage[`${name}`] = val;
        setStuImage(_stuImage);
    }



    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedStudentImg || !selectedStudentImg.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>

                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const codeBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }
    // const categoryBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Image category</span>
    //             {rowData.heading}
    //         </>
    //     );
    // }

    const imageBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Image</span>
                <img src={`assets/demo/images/lifeAtJaypee/${rowData.lifecampusstuimg}`} alt={rowData.lifecampusstuimg} className="shadow-2" width="200" />
            </>
        )
    }

    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => editstuImage(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeletestuImage(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">Life On Campus</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const studentDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={savestuImage} />
        </>
    );
    const deleteStudentDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteStudentDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deletestuImage} />
        </>
    );
    const deleteStuImageDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteStuImageDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedStudentImg} />
        </>
    );



    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={studentImg} selection={selectedStudentImg} onSelectionChange={(e) => setSelectedStudentImg(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} studentImg"
                        globalFilter={globalFilter} emptyMessage="No studentImg found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                        <Column field="Id" header="Id" sortable body={codeBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column header="Image" body={imageBodyTemplate} headerStyle={{ width: '100%', minWidth: '10rem' }}></Column>
                        {/* <Column field="Image category" header="Image Category" body={categoryBodyTemplate} style={{ width: "100%" }} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column> */}

                        <Column header="Action" body={actionBodyTemplate} style={{ paddingBottom: "22px", width: "100%" }}></Column>
                    </DataTable>

                    <Dialog visible={studentDialog} style={{ width: '450px' }} header="Carnival Picture" modal className="p-fluid" footer={studentDialogFooter} onHide={hideDialog}>
                        <div className="field">
                            <FileUpload name="demo[]" url="./upload" chooseOptions={chooseOptions} uploadOptions={uploadOptions} cancelOptions={cancelOptions} />
                        </div>
                    </Dialog>

                    <Dialog visible={deleteStudentDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteStudentDialogFooter} onHide={hidedeleteStudentDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {stuImage && <span>Are you sure you want to delete <b>{stuImage.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteStuImageDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteStuImageDialogFooter} onHide={hidedeleteStuImageDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {stuImage && <span>Are you sure you want to delete the selected studentImg?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(LifeAtCampus, comparisonFn);