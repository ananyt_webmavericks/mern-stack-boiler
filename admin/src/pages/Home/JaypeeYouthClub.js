import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Jaypee } from '../../service/Home/Jaypee';


const JaypeeYouthClub = () => {

    const chooseOptions = { label: 'Choose', icon: 'pi pi-fw pi-plus' };
    const uploadOptions = { label: 'Uplaod', icon: 'pi pi-upload', className: 'p-button-success' };
    const cancelOptions = { label: 'Cancel', icon: 'pi pi-times', className: 'p-button-danger' };
    let emptyjaypee = {
        id: null,
        Annualfesthomeid:null,
        Annualfesthomeimg:'',
    };

    const [jaypee, setJaypee] = useState(null);
    const [jaypeeDialog, setJaypeeDialog] = useState(false);
    const [deleteJaypeeDialog, setDeleteJaypeeDialog] = useState(false);
    const [deleteYouthClubDialog, setDeleteYouthClubDialog] = useState(false);
    const [youthClub, setYouthClub] = useState(emptyjaypee);
    const [selectedJaypee, setSelectedJaypee] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const jaypee = new Jaypee();
        jaypee.getJaypee().then(data => setJaypee(data));
    }, []);


    const openNew = () => {
        setYouthClub(emptyjaypee);
        setSubmitted(false);
        setJaypeeDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setJaypeeDialog(false);
    }

    const hidedeleteJaypeeDialog = () => {
        setDeleteJaypeeDialog(false);
    }

    const hidedeleteYouthClubDialog = () => {
        setDeleteYouthClubDialog(false);
    }

    const saveyouthClub = () => {
        setSubmitted(true);

        if (youthClub.name.trim()) {
            let _jaypee = [...jaypee];
            let _youthClub = { ...youthClub };
            if (youthClub.id) {
                const index = findIndexById(youthClub.id);

                _jaypee[index] = _youthClub;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'youthClub Updated', life: 3000 });
            }
            else {
                _youthClub.id = createId();
                _youthClub.image = 'youthClub-placeholder.svg';
                _jaypee.push(_youthClub);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'youthClub Created', life: 3000 });
            }

            setJaypee(_jaypee);
            setJaypeeDialog(false);
            setYouthClub(emptyjaypee);
        }
    }

    const edityouthClub = (youthClub) => {
        setYouthClub({ ...youthClub });
        setJaypeeDialog(true);
    }

    const confirmDeleteyouthClub = (youthClub) => {
        setYouthClub(youthClub);
        setDeleteJaypeeDialog(true);
    }

    const deleteyouthClub = () => {
        let _jaypee = jaypee.filter(val => val.id !== youthClub.id);
        setJaypee(_jaypee);
        setDeleteJaypeeDialog(false);
        setYouthClub(emptyjaypee);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'youthClub Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < jaypee.length; i++) {
            if (jaypee[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteYouthClubDialog(true);
    }

    const deleteselectedJaypee = () => {
        let _jaypee = jaypee.filter(val => !selectedJaypee.includes(val));
        setJaypee(_jaypee);
        setDeleteYouthClubDialog(false);
        setSelectedJaypee(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'jaypee Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _youthClub = { ...youthClub };
        _youthClub[`${name}`] = val;
        setYouthClub(_youthClub);
    }



    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedJaypee || !selectedJaypee.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>

                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const codeBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }
    // const categoryBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Image category</span>
    //             {rowData.heading}
    //         </>
    //     );
    // }

    const imageBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Image</span>
                <img src={`assets/demo/images/jaypee/${rowData.Annualfesthomeimg}`} alt={rowData.Annualfesthomeimg} className="shadow-2" width="200" />
            </>
        )
    }

    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => edityouthClub(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeleteyouthClub(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">jaypee Images</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const jaypeeDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={saveyouthClub} />
        </>
    );
    const deleteJaypeeDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteJaypeeDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteyouthClub} />
        </>
    );
    const deleteYouthClubDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteYouthClubDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedJaypee} />
        </>
    );



    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={jaypee} selection={selectedJaypee} onSelectionChange={(e) => setSelectedJaypee(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} jaypee"
                        globalFilter={globalFilter} emptyMessage="No jaypee found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                        <Column field="Id" header="Id" sortable body={codeBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column header="Image" body={imageBodyTemplate} headerStyle={{ width: '100%', minWidth: '10rem' }}></Column>
                        {/* <Column field="Image category" header="Image Category" body={categoryBodyTemplate} style={{ width: "100%" }} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column> */}

                        <Column header="Action" body={actionBodyTemplate} style={{ paddingBottom: "22px", width: "100%" }}></Column>
                    </DataTable>

                    <Dialog visible={jaypeeDialog} style={{ width: '450px' }} header="Carnival Picture" modal className="p-fluid" footer={jaypeeDialogFooter} onHide={hideDialog}>
                        <div className="field">
                            <FileUpload name="demo[]" url="./upload" chooseOptions={chooseOptions} uploadOptions={uploadOptions} cancelOptions={cancelOptions} />
                        </div>
                    </Dialog>

                    <Dialog visible={deleteJaypeeDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteJaypeeDialogFooter} onHide={hidedeleteJaypeeDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {youthClub && <span>Are you sure you want to delete <b>{youthClub.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteYouthClubDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteYouthClubDialogFooter} onHide={hidedeleteYouthClubDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {youthClub && <span>Are you sure you want to delete the selected jaypee?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(JaypeeYouthClub, comparisonFn);