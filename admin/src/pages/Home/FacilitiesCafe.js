import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Facilities } from '../../service/Home/Facilities';


const FacilitiesCafe = () => {

    const chooseOptions = { label: 'Choose', icon: 'pi pi-fw pi-plus' };
    const uploadOptions = { label: 'Uplaod', icon: 'pi pi-upload', className: 'p-button-success' };
    const cancelOptions = { label: 'Cancel', icon: 'pi pi-times', className: 'p-button-danger' };
    let emptycafe = {
        id: null,
        cafeId:null,
        CafeImage:'',
    };

    const [cafe, setCafe] = useState(null);
    const [cafeDialog, setCafeDialog] = useState(false);
    const [deleteCafeDialog, setDeleteCafeDialog] = useState(false);
    const [deleteFacCafeDialog, setDeleteFacCafeDialog] = useState(false);
    const [facCafe, setFacCafe] = useState(emptycafe);
    const [selectedCafe, setSelectedCafe] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const facilities = new Facilities();
        facilities.getFacilitiesCafe().then(data => setCafe(data));
    }, []);


    const openNew = () => {
        setFacCafe(emptycafe);
        setSubmitted(false);
        setCafeDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setCafeDialog(false);
    }

    const hidedeleteCafeDialog = () => {
        setDeleteCafeDialog(false);
    }

    const hidedeleteFacCafeDialog = () => {
        setDeleteFacCafeDialog(false);
    }

    const savefacCafe = () => {
        setSubmitted(true);

        if (facCafe.name.trim()) {
            let _cafe = [...cafe];
            let _facCafe = { ...facCafe };
            if (facCafe.id) {
                const index = findIndexById(facCafe.id);

                _cafe[index] = _facCafe;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'facCafe Updated', life: 3000 });
            }
            else {
                _facCafe.id = createId();
                _facCafe.image = 'facCafe-placeholder.svg';
                _cafe.push(_facCafe);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'facCafe Created', life: 3000 });
            }

            setCafe(_cafe);
            setCafeDialog(false);
            setFacCafe(emptycafe);
        }
    }

    const editfacCafe = (facCafe) => {
        setFacCafe({ ...facCafe });
        setCafeDialog(true);
    }

    const confirmDeletefacCafe = (facCafe) => {
        setFacCafe(facCafe);
        setDeleteCafeDialog(true);
    }

    const deletefacCafe = () => {
        let _cafe = cafe.filter(val => val.id !== facCafe.id);
        setCafe(_cafe);
        setDeleteCafeDialog(false);
        setFacCafe(emptycafe);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'facCafe Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < cafe.length; i++) {
            if (cafe[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteFacCafeDialog(true);
    }

    const deleteselectedCafe = () => {
        let _cafe = cafe.filter(val => !selectedCafe.includes(val));
        setCafe(_cafe);
        setDeleteFacCafeDialog(false);
        setSelectedCafe(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'cafe Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _facCafe = { ...facCafe };
        _facCafe[`${name}`] = val;
        setFacCafe(_facCafe);
    }



    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedCafe || !selectedCafe.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>

                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const codeBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }
    // const categoryBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Image category</span>
    //             {rowData.heading}
    //         </>
    //     );
    // }

    const imageBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Image</span>
                <img src={`assets/demo/images/cafe/${rowData.CafeImage}`} alt={rowData.CafeImage} className="shadow-2" width="200" />
            </>
        )
    }

    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => editfacCafe(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeletefacCafe(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">cafe Images</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const cafeDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={savefacCafe} />
        </>
    );
    const deleteCafeDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteCafeDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deletefacCafe} />
        </>
    );
    const deleteFacCafeDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteFacCafeDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedCafe} />
        </>
    );



    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={cafe} selection={selectedCafe} onSelectionChange={(e) => setSelectedCafe(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} cafe"
                        globalFilter={globalFilter} emptyMessage="No cafe found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                        <Column field="Id" header="Id" sortable body={codeBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column header="Image" body={imageBodyTemplate} headerStyle={{ width: '100%', minWidth: '10rem' }}></Column>
                        {/* <Column field="Image category" header="Image Category" body={categoryBodyTemplate} style={{ width: "100%" }} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column> */}

                        <Column header="Action" body={actionBodyTemplate} style={{ paddingBottom: "22px", width: "100%" }}></Column>
                    </DataTable>

                    <Dialog visible={cafeDialog} style={{ width: '450px' }} header="Carnival Picture" modal className="p-fluid" footer={cafeDialogFooter} onHide={hideDialog}>
                        <div className="field">
                            <FileUpload name="demo[]" url="./upload" chooseOptions={chooseOptions} uploadOptions={uploadOptions} cancelOptions={cancelOptions} />
                        </div>
                    </Dialog>

                    <Dialog visible={deleteCafeDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteCafeDialogFooter} onHide={hidedeleteCafeDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {facCafe && <span>Are you sure you want to delete <b>{facCafe.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteFacCafeDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteFacCafeDialogFooter} onHide={hidedeleteFacCafeDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {facCafe && <span>Are you sure you want to delete the selected cafe?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(FacilitiesCafe, comparisonFn);