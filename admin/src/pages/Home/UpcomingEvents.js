import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { InputTextarea } from 'primereact/inputtextarea';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Upcoming } from '../../service/Home/Upcoming';


const UpcomingEvents = () => {


    let emptyEvents = {
        id: null,
        List: '',
        ListTo: '',
    };

    const [eventsList, setEventsList] = useState(null);
    const [eventDialog, setEventDialog] = useState(false);
    const [deleteEventDialog, setDeleteEventDialog] = useState(false);
    const [deleteEventListDialog, setDeleteEventListDialog] = useState(false);
    const [eventList, setEventList] = useState(emptyEvents);
    const [selectedEventsList, setSelectedEventsList] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const upcoming = new Upcoming();
        upcoming.getUpcoming().then(data => setEventsList(data));
    }, []);

    const formatCurrency = (value) => {
        return value.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
    }

    const openNew = () => {
        setEventList(emptyEvents);
        setSubmitted(false);
        setEventDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setEventDialog(false);
    }

    const hidedeleteEventDialog = () => {
        setDeleteEventDialog(false);
    }

    const hidedeleteEventListDialog = () => {
        setDeleteEventListDialog(false);
    }

    const saveeventList = () => {
        setSubmitted(true);

        if (eventList.name.trim()) {
            let _eventsList = [...eventsList];
            let _eventList = { ...eventList };
            if (eventList.id) {
                const index = findIndexById(eventList.id);

                _eventsList[index] = _eventList;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'eventList Updated', life: 3000 });
            }
            else {
                _eventList.id = createId();
                _eventList.image = 'eventList-placeholder.svg';
                _eventsList.push(_eventList);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'eventList Created', life: 3000 });
            }

            setEventsList(_eventsList);
            setEventDialog(false);
            setEventList(emptyEvents);
        }
    }

    const editeventList = (eventList) => {
        setEventList({ ...eventList });
        setEventDialog(true);
    }

    const confirmDeleteeventList = (eventList) => {
        setEventList(eventList);
        setDeleteEventDialog(true);
    }

    const deleteeventList = () => {
        let _eventsList = eventsList.filter(val => val.id !== eventList.id);
        setEventsList(_eventsList);
        setDeleteEventDialog(false);
        setEventList(emptyEvents);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'eventList Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < eventsList.length; i++) {
            if (eventsList[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteEventListDialog(true);
    }

    const deleteselectedEventsList = () => {
        let _eventsList = eventsList.filter(val => !selectedEventsList.includes(val));
        setEventsList(_eventsList);
        setDeleteEventListDialog(false);
        setSelectedEventsList(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'eventsList Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _eventList = { ...eventList };
        _eventList[`${name}`] = val;
        setEventList(_eventList);
    }



    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedEventsList || !selectedEventsList.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>

                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const codeBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }

    const nameBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Events</span>
                {rowData.List}
            </>
        );
    }


    const priceBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">LinkTo</span>
                {formatCurrency(rowData.ListTo)}
            </>
        );
    }

    // const categoryBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Department</span>
    //             {rowData.dpmt}
    //         </>
    //     );
    // }

    // const ratingBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Readmore</span>
    //             <Rating value={rowData.readmore} readonly cancel={false} />
    //         </>
    //     );
    // }


    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => editeventList(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeleteeventList(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">Important Events List</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const eventDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={saveeventList} />
        </>
    );
    const deleteEventDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteEventDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteeventList} />
        </>
    );
    const deleteEventListDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteEventListDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedEventsList} />
        </>
    );



    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={eventsList} selection={selectedEventsList} onSelectionChange={(e) => setSelectedEventsList(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} eventsList"
                        globalFilter={globalFilter} emptyMessage="No eventsList found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                        <Column field="Id" header="Id" sortable body={codeBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column field="events" header="events" sortable body={nameBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column field="Link TO" header="Link TO" body={priceBodyTemplate} style={{ width: "100%" }} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        {/* <Column field="inventoryStatus" header="Status" body={statusBodyTemplate} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column> */}
                        <Column header="Action" body={actionBodyTemplate} style={{ float: "right",  paddingBottom: "22px" ,width: "100%" }}></Column>
                    </DataTable>

                    <Dialog visible={eventDialog} style={{ width: '450px' }} header="Technical Staff" modal className="p-fluid" footer={eventDialogFooter} onHide={hideDialog}>
                    <div className="field">
                    <FileUpload  mode = "basic" accept="pdf/*"  label="Upload File" chooseLabel="Upload File" className="mr-2 inline-block"/>
 
                    </div>
                    
                        <div className="field">
                            <label htmlFor="name">events</label>
                            <InputTextarea id="events" value={eventList.List} onChange={(e) => onInputChange(e, 'List')} required autoFocus className={classNames({ 'p-invalid': submitted && !eventList.List })} />
                            {submitted && !eventList.List && <small className="p-invalid">Events is required.</small>}
                        </div>
                        
                        <div className="field">
                            <label htmlFor="title">link To</label>
                            <InputText id="linkTo" value={eventList.ListTo} onChange={(e) => onInputChange(e, 'title')} required autoFocus className={classNames({ 'p-invalid': submitted && !eventList.ListTo })} />

                        </div>
                    </Dialog>

                    <Dialog visible={deleteEventDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteEventDialogFooter} onHide={hidedeleteEventDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {eventList && <span>Are you sure you want to delete <b>{eventList.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteEventListDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteEventListDialogFooter} onHide={hidedeleteEventListDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {eventList && <span>Are you sure you want to delete the selected eventsList?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(UpcomingEvents, comparisonFn);