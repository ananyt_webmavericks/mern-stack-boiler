import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { InputTextarea } from 'primereact/inputtextarea';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Highlights } from '../../service/Home/Highlights';

const HighlightPage = () => {


    let emptyhighlightsp = {
        id: null,
        stuid:null,
        stuName: '',
        stuCourse: '',
        stuPlaced:'',
        stuImage:'',
    };

    const [highlightList, setHighlightList] = useState(null);
    const [highlightDialog, setHighlightDialog] = useState(false);
    const [deleteHighlightDialog, setDeleteHighlightDialog] = useState(false);
    const [deleteHighlightListDialog, setdeleteHighlightListDialog] = useState(false);
    const [highlightsp, setHighlightsp] = useState(emptyhighlightsp);
    const [selectedHighlightList, setSelectedHighlightList] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const highlights = new Highlights();
        highlights.getHighlights().then(data => setHighlightList(data));
    }, []);

    const formatCurrency = (value) => {
        return value.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
    }

    const openNew = () => {
        setHighlightsp(emptyhighlightsp);
        setSubmitted(false);
        setHighlightDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setHighlightDialog(false);
    }

    const hidedeleteHighlightDialog = () => {
        setDeleteHighlightDialog(false);
    }

    const hidedeleteHighlightListDialog = () => {
        setdeleteHighlightListDialog(false);
    }

    const savehighlightsp = () => {
        setSubmitted(true);

        if (highlightsp.name.trim()) {
            let _highlightList = [...highlightList];
            let _highlightsp = { ...highlightsp };
            if (highlightsp.id) {
                const index = findIndexById(highlightsp.id);

                _highlightList[index] = _highlightsp;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'highlightsp Updated', life: 3000 });
            }
            else {
                _highlightsp.id = createId();
                _highlightsp.image = 'highlightsp-placeholder.svg';
                _highlightList.push(_highlightsp);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'highlightsp Created', life: 3000 });
            }

            setHighlightList(_highlightList);
            setHighlightDialog(false);
            setHighlightsp(emptyhighlightsp);
        }
    }

    const edithighlightsp = (highlightsp) => {
        setHighlightsp({ ...highlightsp });
        setHighlightDialog(true);
    }

    const confirmDeletehighlightsp = (highlightsp) => {
        setHighlightsp(highlightsp);
        setDeleteHighlightDialog(true);
    }

    const deletehighlightsp = () => {
        let _highlightList = highlightList.filter(val => val.id !== highlightsp.id);
        setHighlightList(_highlightList);
        setDeleteHighlightDialog(false);
        setHighlightsp(emptyhighlightsp);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'highlightsp Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < highlightList.length; i++) {
            if (highlightList[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setdeleteHighlightListDialog(true);
    }

    const deleteselectedHighlightList = () => {
        let _highlightList = highlightList.filter(val => !selectedHighlightList.includes(val));
        setHighlightList(_highlightList);
        setdeleteHighlightListDialog(false);
        setSelectedHighlightList(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'highlightList Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _highlightsp = { ...highlightsp };
        _highlightsp[`${name}`] = val;
        setHighlightsp(_highlightsp);
    }



    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedHighlightList || !selectedHighlightList.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>

                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const codeBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }

    const nameBodyTemplate1 = (rowData) => {
        return (
            <>
                <span className="p-column-title">Announcements</span>
                {rowData.stuName}
            </>
        );
    }
    const nameBodyTemplate2 = (rowData) => {
        return (
            <>
                <span className="p-column-title">Announcements</span>
                {rowData.stuCourse}
            </>
        );
    }
    const nameBodyTemplate3 = (rowData) => {
        return (
            <>
                <span className="p-column-title">Announcements</span>
                {rowData.stuPlaced}
            </>
        );
    }

    const imageBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">LinkTo</span>
                <img src={`assets/demo/images/proud_student/${rowData.stuImage}`} alt={rowData.stuImage} className="shadow-2" width="100" />
            </>
        )
    }

    // const priceBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">LinkTo</span>
    //             {formatCurrency(rowData.linkTo)}
    //         </>
    //     );
    // }

    // const categoryBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Department</span>
    //             {rowData.dpmt}
    //         </>
    //     );
    // }

    // const ratingBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Readmore</span>
    //             <Rating value={rowData.readmore} readonly cancel={false} />
    //         </>
    //     );
    // }


    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => edithighlightsp(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeletehighlightsp(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">Proud Students</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const highlightDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={savehighlightsp} />
        </>
    );
    const deleteHighlightDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteHighlightDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deletehighlightsp} />
        </>
    );
    const deleteHighlightListDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteHighlightListDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedHighlightList} />
        </>
    );



    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={highlightList} selection={selectedHighlightList} onSelectionChange={(e) => setSelectedHighlightList(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} highlightList"
                        globalFilter={globalFilter} emptyMessage="No highlightList found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                        <Column field="Id" header="Id" sortable body={codeBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column field="Name" header="Name" sortable body={nameBodyTemplate1} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column field="Course Done" header="Course Done" sortable body={nameBodyTemplate2} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column field="Placed At" header="Placed At" sortable body={nameBodyTemplate3} headerStyle={{ width: '100%', minWidth: '10rem' }}></Column>
                        <Column field="Image" header="Image" body={imageBodyTemplate} style={{ width: "14%" }} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        {/* <Column field="inventoryStatus" header="Status" body={statusBodyTemplate} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column> */}
                        <Column header="Action" body={actionBodyTemplate} style={{ float: "right",  paddingBottom: "22px" ,width: "100%" }}></Column>
                    </DataTable>

                    <Dialog visible={highlightDialog} style={{ width: '450px' }} header="Technical Staff" modal className="p-fluid" footer={highlightDialogFooter} onHide={hideDialog}>
                    <div className="field">
                    <FileUpload  mode = "basic" accept="pdf/*"  label="Upload File" chooseLabel="Upload File" className="mr-2 inline-block"/>
 
                    </div>
                        {/* <div className="field">
                            <label htmlFor="name">Announcement</label>
                            <InputTextarea id="announcement" value={highlightsp.announcement} onChange={(e) => onInputChange(e, 'name')} required autoFocus className={classNames({ 'p-invalid': submitted && !highlightsp.announcement })} />
                            {submitted && !highlightsp.name && <small className="p-invalid">Announcement is required.</small>}
                        </div> */}
                        <div className="field">
                            <label htmlFor="title">Student Name</label>
                            <InputText id="stuName" value={highlightsp.stuName} onChange={(e) => onInputChange(e, 'title')} required autoFocus className={classNames({ 'p-invalid': submitted && !highlightsp.stuName })} />

                        </div>
                        <div className="field">
                            <label htmlFor="stuCourse">course</label>
                            <InputText id="stuCourse" value={highlightsp.stuCourse} onChange={(e) => onInputChange(e, 'title')} required autoFocus className={classNames({ 'p-invalid': submitted && !highlightsp.stuName })} />

                        </div>
                        <div className="field">
                            <label htmlFor="stuPlaced">Placed At</label>
                            <InputText id="stuPlaced" value={highlightsp.stuPlaced} onChange={(e) => onInputChange(e, 'title')} required autoFocus className={classNames({ 'p-invalid': submitted && !highlightsp.stuPlaced })} />

                        </div>

                    </Dialog>

                    <Dialog visible={deleteHighlightDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteHighlightDialogFooter} onHide={hidedeleteHighlightDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {highlightsp && <span>Are you sure you want to delete <b>{highlightsp.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteHighlightListDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteHighlightListDialogFooter} onHide={hidedeleteHighlightListDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {highlightsp && <span>Are you sure you want to delete the selected highlightList?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(HighlightPage, comparisonFn);