import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Facilities } from '../../service/Home/Facilities';


const FacilitiesHostel = () => {

    const chooseOptions = { label: 'Choose', icon: 'pi pi-fw pi-plus' };
    const uploadOptions = { label: 'Uplaod', icon: 'pi pi-upload', className: 'p-button-success' };
    const cancelOptions = { label: 'Cancel', icon: 'pi pi-times', className: 'p-button-danger' };
    let emptyhostel = {
        id: null,
        hostelId:null,
        hostelImage:'',
    };

    const [hostel, setHostel] = useState(null);
    const [hostelDialog, setHostelDialog] = useState(false);
    const [deleteHostelDialog, setDeleteHostelDialog] = useState(false);
    const [deleteFacHostelDialog, setDeleteFacHostelDialog] = useState(false);
    const [facHostel, setFacHostel] = useState(emptyhostel);
    const [selectedHostel, setSelectedHostel] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const facilities = new Facilities();
        facilities.getFacilitiesHostel().then(data => setHostel(data));
    }, []);


    const openNew = () => {
        setFacHostel(emptyhostel);
        setSubmitted(false);
        setHostelDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setHostelDialog(false);
    }

    const hidedeleteHostelDialog = () => {
        setDeleteHostelDialog(false);
    }

    const hidedeleteFacHostelDialog = () => {
        setDeleteFacHostelDialog(false);
    }

    const savefacHostel = () => {
        setSubmitted(true);

        if (facHostel.name.trim()) {
            let _hostel = [...hostel];
            let _facHostel = { ...facHostel };
            if (facHostel.id) {
                const index = findIndexById(facHostel.id);

                _hostel[index] = _facHostel;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'facHostel Updated', life: 3000 });
            }
            else {
                _facHostel.id = createId();
                _facHostel.image = 'facHostel-placeholder.svg';
                _hostel.push(_facHostel);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'facHostel Created', life: 3000 });
            }

            setHostel(_hostel);
            setHostelDialog(false);
            setFacHostel(emptyhostel);
        }
    }

    const editfacHostel = (facHostel) => {
        setFacHostel({ ...facHostel });
        setHostelDialog(true);
    }

    const confirmDeletefacHostel = (facHostel) => {
        setFacHostel(facHostel);
        setDeleteHostelDialog(true);
    }

    const deletefacHostel = () => {
        let _hostel = hostel.filter(val => val.id !== facHostel.id);
        setHostel(_hostel);
        setDeleteHostelDialog(false);
        setFacHostel(emptyhostel);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'facHostel Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < hostel.length; i++) {
            if (hostel[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteFacHostelDialog(true);
    }

    const deleteselectedHostel = () => {
        let _hostel = hostel.filter(val => !selectedHostel.includes(val));
        setHostel(_hostel);
        setDeleteFacHostelDialog(false);
        setSelectedHostel(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'hostel Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _facHostel = { ...facHostel };
        _facHostel[`${name}`] = val;
        setFacHostel(_facHostel);
    }



    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedHostel || !selectedHostel.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>

                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const codeBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }
    // const categoryBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Image category</span>
    //             {rowData.heading}
    //         </>
    //     );
    // }

    const imageBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Image</span>
                <img src={`assets/demo/images/hostel/${rowData.hostelImage}`} alt={rowData.hostelImage} className="shadow-2" width="200" />
            </>
        )
    }

    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => editfacHostel(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeletefacHostel(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">hostel Images</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const hostelDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={savefacHostel} />
        </>
    );
    const deleteHostelDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteHostelDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deletefacHostel} />
        </>
    );
    const deleteFacHostelDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteFacHostelDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedHostel} />
        </>
    );



    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={hostel} selection={selectedHostel} onSelectionChange={(e) => setSelectedHostel(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} hostel"
                        globalFilter={globalFilter} emptyMessage="No hostel found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                        <Column field="Id" header="Id" sortable body={codeBodyTemplate} headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        <Column header="Image" body={imageBodyTemplate} headerStyle={{ width: '100%', minWidth: '10rem' }}></Column>
                        {/* <Column field="Image category" header="Image Category" body={categoryBodyTemplate} style={{ width: "100%" }} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column> */}

                        <Column header="Action" body={actionBodyTemplate} style={{ paddingBottom: "22px", width: "100%" }}></Column>
                    </DataTable>

                    <Dialog visible={hostelDialog} style={{ width: '450px' }} header="Carnival Picture" modal className="p-fluid" footer={hostelDialogFooter} onHide={hideDialog}>
                        <div className="field">
                            <FileUpload name="demo[]" url="./upload" chooseOptions={chooseOptions} uploadOptions={uploadOptions} cancelOptions={cancelOptions} />
                        </div>
                    </Dialog>

                    <Dialog visible={deleteHostelDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteHostelDialogFooter} onHide={hidedeleteHostelDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {facHostel && <span>Are you sure you want to delete <b>{facHostel.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteFacHostelDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteFacHostelDialogFooter} onHide={hidedeleteFacHostelDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {facHostel && <span>Are you sure you want to delete the selected hostel?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(FacilitiesHostel, comparisonFn);