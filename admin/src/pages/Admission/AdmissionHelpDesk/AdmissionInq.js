import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { InputTextarea } from 'primereact/inputtextarea';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { AdmissionInquiries } from '../../../service/Admission/AdmissionInquiries';



const AdmissionInq = () => {


    let emptyAdmission = {
        id: null,
        inqrId:null,
        inqrName: '',
        inqrDesignation: '',
        inqrContactNo1:'',
        inqrContactNo2:'',
    };

    const [procedure, setProcedure] = useState(null);
    const [proceedDialog, setProceedDialog] = useState(false);
    const [deleteProceedDialog, setDeleteProceedDialog] = useState(false);
    const [deleteProcedureDialog, setDeleteProcedureDialog] = useState(false);
    const [admission, setAdmission] = useState(emptyAdmission);
    const [selectedProcedure, setSelectedProcedure] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    const dt = useRef(null);

    useEffect(() => {
        const admissionInquiries = new AdmissionInquiries();
        admissionInquiries.getAdmissionInq().then(data => setProcedure(data));
    }, []);

    const formatCurrency = (value) => {
        return value.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
    }

    const openNew = () => {
        setAdmission(emptyAdmission);
        setSubmitted(false);
        setProceedDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setProceedDialog(false);
    }

    const hidedeleteProceedDialog = () => {
        setDeleteProceedDialog(false);
    }

    const hidedeleteProcedureDialog = () => {
        setDeleteProcedureDialog(false);
    }

    const saveadmission = () => {
        setSubmitted(true);

        if (admission.name.trim()) {
            let _procedure = [...procedure];
            let _admission = { ...admission };
            if (admission.id) {
                const index = findIndexById(admission.id);

                _procedure[index] = _admission;
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'admission Updated', life: 3000 });
            }
            else {
                _admission.id = createId();
                _admission.image = 'admission-placeholder.svg';
                _procedure.push(_admission);
                toast.current.show({ severity: 'success', summary: 'Successful', detail: 'admission Created', life: 3000 });
            }

            setProcedure(_procedure);
            setProceedDialog(false);
            setAdmission(emptyAdmission);
        }
    }

    const editadmission = (admission) => {
        setAdmission({ ...admission });
        setProceedDialog(true);
    }

    const confirmDeleteadmission = (admission) => {
        setAdmission(admission);
        setDeleteProceedDialog(true);
    }

    const deleteadmission = () => {
        let _procedure = procedure.filter(val => val.id !== admission.id);
        setProcedure(_procedure);
        setDeleteProceedDialog(false);
        setAdmission(emptyAdmission);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'admission Deleted', life: 3000 });
    }

    const findIndexById = (id) => {
        let index = -1;
        for (let i = 0; i < procedure.length; i++) {
            if (procedure[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    const createId = () => {
        let id = '';
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }

    const exportCSV = () => {
        dt.current.exportCSV();
    }

    const confirmDeleteSelected = () => {
        setDeleteProcedureDialog(true);
    }

    const deleteselectedProcedure = () => {
        let _procedure = procedure.filter(val => !selectedProcedure.includes(val));
        setProcedure(_procedure);
        setDeleteProcedureDialog(false);
        setSelectedProcedure(null);
        toast.current.show({ severity: 'success', summary: 'Successful', detail: 'procedure Deleted', life: 3000 });
    }

    const onInputChange = (e, name) => {
        const val = (e.target && e.target.value) || '';
        let _admission = { ...admission };
        _admission[`${name}`] = val;
        setAdmission(_admission);
    }



    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <div className="my-2">
                    <Button label="New" icon="pi pi-plus" className="p-button-success mr-2" onClick={openNew} />
                    <Button label="Delete" icon="pi pi-trash" className="p-button-danger" onClick={confirmDeleteSelected} disabled={!selectedProcedure || !selectedProcedure.length} />
                </div>
            </React.Fragment>
        )
    }

    const rightToolbarTemplate = () => {
        return (
            <React.Fragment>

                <Button label="Export" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
            </React.Fragment>
        )
    }


    const codeBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Id</span>
                {rowData.id}
            </>
        );
    }

    const nameBodyTemplate = (rowData) => {
        return (
            <>
                <span className="p-column-title">Name</span>
                {rowData.inqrName}
            </>
        );
    }

    // const imageBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">LinkTo</span>
    //             <img src={`assets/demo/images/staff/${rowData.image}`} alt={rowData.image} className="shadow-2" width="100" />
    //         </>
    //     )
    // }

    const BodyTemplate1 = (rowData) => {
        return (
            <>
                <span className="p-column-title">Designation</span>
                {formatCurrency(rowData.inqrDesignation)}
            </>
        );
    }
    const BodyTemplate2 = (rowData) => {
        return (
            <>
                <span className="p-column-title">Cantact 1</span>
                {formatCurrency(rowData.inqrContactNo1)}
            </>
        );
    }
    const BodyTemplate3 = (rowData) => {
        return (
            <>
                <span className="p-column-title">Contact 2</span>
                {formatCurrency(rowData.inqrContactNo2)}
            </>
        );
    }



    // const categoryBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Department</span>
    //             {rowData.dpmt}
    //         </>
    //     );
    // }

    // const ratingBodyTemplate = (rowData) => {
    //     return (
    //         <>
    //             <span className="p-column-title">Readmore</span>
    //             <Rating value={rowData.readmore} readonly cancel={false} />
    //         </>
    //     );
    // }


    const actionBodyTemplate = (rowData) => {
        return (
            <div className="actions">
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-warning mr-2" onClick={() => editadmission(rowData)} />
                <Button icon="pi pi-trash" className="p-button-rounded p-button-danger mt-2" onClick={() => confirmDeleteadmission(rowData)} />
            </div>
        );
    }

    const header = (
        <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
            <h5 className="m-0">procedure Openings</h5>
            <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );

    const proceedDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={saveadmission} />
        </>
    );
    const deleteProceedDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteProceedDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteadmission} />
        </>
    );
    const deleteProcedureDialogFooter = (
        <>
            <Button label="No" icon="pi pi-times" className="p-button-text" onClick={hidedeleteProcedureDialog} />
            <Button label="Yes" icon="pi pi-check" className="p-button-text" onClick={deleteselectedProcedure} />
        </>
    );



    return (
        <div className="grid crud-demo">
            <div className="col-12">
                <div className="card">
                    <Toast ref={toast} />
                    <Toolbar className="mb-4" left={leftToolbarTemplate} right={rightToolbarTemplate}></Toolbar>

                    <DataTable ref={dt} value={procedure} selection={selectedProcedure} onSelectionChange={(e) => setSelectedProcedure(e.value)}
                        dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]} className="datatable-responsive"
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} procedure"
                        globalFilter={globalFilter} emptyMessage="No procedure found." header={header} responsiveLayout="scroll">
                        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                        <Column field="Id" header="Id" sortable body={codeBodyTemplate} headerStyle={{ width: '10%', minWidth: '10rem' }}></Column>
                        <Column field="Name" header="Name" sortable body={nameBodyTemplate} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column>
                        <Column field="Designation" header="Designation" sortable body={BodyTemplate1} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column>
                        <Column field="Contact Number 1" header="Contact Number 2" sortable body={BodyTemplate2} headerStyle={{ width: '20%', minWidth: '10rem' }}></Column>
                        <Column field="Contact Number 2" header="Contact Number 2" body={BodyTemplate3} style={{ width: "20%" }} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column>
                        {/* <Column field="inventoryStatus" header="Status" body={statusBodyTemplate} sortable headerStyle={{ width: '14%', minWidth: '10rem' }}></Column> */}
                        <Column header="Action" body={actionBodyTemplate} style={{ float: "right",  paddingBottom: "22px" ,width: "100%" }}></Column>
                    </DataTable>

                    <Dialog visible={proceedDialog} style={{ width: '450px' }} header="Technical Staff" modal className="p-fluid" footer={proceedDialogFooter} onHide={hideDialog}>
                    <div className="field">
                    <FileUpload  mode = "basic" accept="pdf/*"  label="Upload File" chooseLabel="Upload File" className="mr-2 inline-block"/>
 
                    </div>
                        <div className="field">
                            <label htmlFor="name">Announcement</label>
                            <InputTextarea id="announcement" value={admission.announcement} onChange={(e) => onInputChange(e, 'name')} required autoFocus className={classNames({ 'p-invalid': submitted && !admission.announcement })} />
                            {submitted && !admission.name && <small className="p-invalid">Announcement is required.</small>}
                        </div>
                        <div className="field">
                            <label htmlFor="title">link To</label>
                            <InputText id="linkTo" value={admission.linkTo} onChange={(e) => onInputChange(e, 'title')} required autoFocus className={classNames({ 'p-invalid': submitted && !admission.linkTo })} />

                        </div>

                    </Dialog>

                    <Dialog visible={deleteProceedDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteProceedDialogFooter} onHide={hidedeleteProceedDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {admission && <span>Are you sure you want to delete <b>{admission.name}</b>?</span>}
                        </div>
                    </Dialog>

                    <Dialog visible={deleteProcedureDialog} style={{ width: '450px' }} header="Confirm" modal footer={deleteProcedureDialogFooter} onHide={hidedeleteProcedureDialog}>
                        <div className="flex align-items-center justify-content-center">
                            <i className="pi pi-exclamation-triangle mr-3" style={{ fontSize: '2rem' }} />
                            {admission && <span>Are you sure you want to delete the selected procedure?</span>}
                        </div>
                    </Dialog>
                </div>
            </div>
        </div>
    );
}

const comparisonFn = function (prevProps, nextProps) {
    return prevProps.location.pathname === nextProps.location.pathname;
};

export default React.memo(AdmissionInq, comparisonFn);