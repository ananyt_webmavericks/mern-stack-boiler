import React, { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { Route, useLocation } from 'react-router-dom';
import { CSSTransition } from 'react-transition-group';

import { AppTopbar } from './AppTopbar';
import { AppFooter } from './AppFooter';
import { AppMenu } from './AppMenu';
import { AppConfig } from './AppConfig';
import { Router , Switch } from 'react-router-dom';


import Dashboard from './components/Dashboard';
import TechnicalStaff from './pages/Academics/TechnicalStaff';
import Announcement from './pages/Home/Announcement';
import AlumniMeet from './pages/Alumni/AlumniMeet';
import AlumniConvo from './pages/Alumni/AlumniConvo';
import AlumniCarnival from './pages/Alumni/AlumniCarnival';
import UpcomingEvents from './pages/Home/UpcomingEvents';
import StudentRecord from './pages/Training/StudentRecord';
import TrainingCell from './pages/Training/TrainingCell';
import HighlightPage from './pages/Home/HighlightPage';
import ButtonDemo from './components/ButtonDemo';
import ChartDemo from './components/ChartDemo';
import Documentation from './components/Documentation';
import FileDemo from './components/FileDemo';
import FloatLabelDemo from './components/FloatLabelDemo';
import FormLayoutDemo from './components/FormLayoutDemo';
import InputDemo from './components/InputDemo';
import ListDemo from './components/ListDemo';
import MenuDemo from './components/MenuDemo';
import MessagesDemo from './components/MessagesDemo';
import MiscDemo from './components/MiscDemo';
import OverlayDemo from './components/OverlayDemo';
import MediaDemo from './components/MediaDemo';
import PanelDemo from './components/PanelDemo';
import TableDemo from './components/TableDemo';
import TreeDemo from './components/TreeDemo';
import InvalidStateDemo from './components/InvalidStateDemo';
import BlocksDemo from './components/BlocksDemo';
import IconsDemo from './components/IconsDemo';
import Infrastructure from './pages/Home/Infrastructure';
import Campus from './pages/Home/Campus';
import JaypeeYouthClub from './pages/Home/JaypeeYouthClub';
import FacilitiesHostel from './pages/Home/FacilitiesHostel';
import FacilitiesCafe from './pages/Home/FacilitiesCafe';
import FacilitiesClass from './pages/Home/FacilitiesClass';
import FacilitiesLib from './pages/Home/FacilitiesLib';
import Account from './pages/About/Account';
import General from './pages/About/General';
import Consultant from './pages/About/Consultant';
import Lrc from './pages/About/Lrc';
import Technician from './pages/About/Technician';
import AcademicCal from './pages/Academics/AcademicCal';
import CareerOpen from './pages/Careers/CareerOpen';
import ThreeYearPro from './pages/Admission/AdmissionProcedure/ThreeYearPro';
import BtechPro from './pages/Admission/AdmissionProcedure/BtechPro';
import LateralPro from './pages/Admission/AdmissionProcedure/LateralPro';
import PhdPro from './pages/Admission/AdmissionProcedure/PhdPro';
import AdmissionInq from './pages/Admission/AdmissionHelpDesk/AdmissionInq';
import StudentRecordChart from './pages/Training/StudentRecordChart';
import LifeAtCampus from './pages/Student/LifeAtCampus';
import Basketball from './pages/Student/Sports/Basketball';
import Cricket from './pages/Student/Sports/Cricket';
import Vollyball from './pages/Student/Sports/Vollyball';
import Football from './pages/Student/Sports/Football';
import Users from './pages/Users/Users';
import Login from './pages/Login/Login';

import Crud from './pages/Crud';
import EmptyPage from './pages/EmptyPage';
import TimelineDemo from './pages/TimelineDemo';

import PrimeReact from 'primereact/api';
import { Tooltip } from 'primereact/tooltip';

import 'primereact/resources/primereact.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import 'prismjs/themes/prism-coy.css';
import './assets/demo/flags/flags.css';
import './assets/demo/Demos.scss';
import './assets/layout/layout.scss';
import './App.scss';

const App = () => {
    const [layoutMode, setLayoutMode] = useState('static');
    const [layoutColorMode, setLayoutColorMode] = useState('light')
    const [inputStyle, setInputStyle] = useState('outlined');
    const [ripple, setRipple] = useState(true);
    const [staticMenuInactive, setStaticMenuInactive] = useState(false);
    const [overlayMenuActive, setOverlayMenuActive] = useState(false);
    const [mobileMenuActive, setMobileMenuActive] = useState(false);
    const [mobileTopbarMenuActive, setMobileTopbarMenuActive] = useState(false);
    const copyTooltipRef = useRef();
    const location = useLocation();

    PrimeReact.ripple = true;

    let menuClick = false;
    let mobileTopbarMenuClick = false;

    useEffect(() => {
        if (mobileMenuActive) {
            addClass(document.body, "body-overflow-hidden");
        } else {
            removeClass(document.body, "body-overflow-hidden");
        }
    }, [mobileMenuActive]);

    useEffect(() => {
        copyTooltipRef && copyTooltipRef.current && copyTooltipRef.current.updateTargetEvents();
    }, [location]);

    const onInputStyleChange = (inputStyle) => {
        setInputStyle(inputStyle);
    }

    const onRipple = (e) => {
        PrimeReact.ripple = e.value;
        setRipple(e.value)
    }

    const onLayoutModeChange = (mode) => {
        setLayoutMode(mode)
    }

    const onColorModeChange = (mode) => {
        setLayoutColorMode(mode)
    }

    const onWrapperClick = (event) => {
        if (!menuClick) {
            setOverlayMenuActive(false);
            setMobileMenuActive(false);
        }

        if (!mobileTopbarMenuClick) {
            setMobileTopbarMenuActive(false);
        }

        mobileTopbarMenuClick = false;
        menuClick = false;
    }

    const onToggleMenuClick = (event) => {
        menuClick = true;

        if (isDesktop()) {
            if (layoutMode === 'overlay') {
                if (mobileMenuActive === true) {
                    setOverlayMenuActive(true);
                }

                setOverlayMenuActive((prevState) => !prevState);
                setMobileMenuActive(false);
            }
            else if (layoutMode === 'static') {
                setStaticMenuInactive((prevState) => !prevState);
            }
        }
        else {
            setMobileMenuActive((prevState) => !prevState);
        }

        event.preventDefault();
    }

    const onSidebarClick = () => {
        menuClick = true;
    }

    const onMobileTopbarMenuClick = (event) => {
        mobileTopbarMenuClick = true;

        setMobileTopbarMenuActive((prevState) => !prevState);
        event.preventDefault();
    }

    const onMobileSubTopbarMenuClick = (event) => {
        mobileTopbarMenuClick = true;

        event.preventDefault();
    }

    const onMenuItemClick = (event) => {
        if (!event.item.items) {
            setOverlayMenuActive(false);
            setMobileMenuActive(false);
        }
    }
    const isDesktop = () => {
        return window.innerWidth >= 992;
    }

    const menu = [
        {
            label: 'Home',
            items: [{
                label: 'Dashboard', icon: 'pi pi-fw pi-home', to: '/app/home'
            },
            { label: 'Users', icon: 'pi pi-fw pi-home', to: '/app/users' }
            ]
        },
        // {
        //     label: 'Dynamic Content',
        //     items: [{
        //         label: 'Technical Staff', icon: 'pi pi-fw pi-home', to: '/TechnicalStaff'
        //     }]

        // },
        {
            label: 'Submenu', icon: 'pi pi-fw pi-bookmark',
            items: [
                {
                    label: 'Home', icon: 'pi pi-fw pi-bookmark',
                    items: [
                        { label: 'Announcement List', icon: 'pi pi-fw pi-bookmark', to: '/app/Announcement' },
                        { label: 'Upcoming Event List', icon: 'pi pi-fw pi-bookmark', to: '/app/UpcomingEvents' },
                        { label: 'Proud Students', icon: 'pi pi-fw pi-bookmark', to: '/app/highlightPage' },
                        {
                            label: 'Life at Jaypee University', icon: 'pi pi-fw pi-bookmark',
                            items: [
                                { label: 'Jaypee Youth Club', icon: 'pi pi-fw pi-bookmark', to: '/app/jaypeeYouthClub' },
                                { label: 'Campus', icon: 'pi pi-fw pi-bookmark', to: '/app/campus' },
                                { label: 'Infrastructure', icon: 'pi pi-fw pi-bookmark', to: '/app/infrastructure' },
                                {
                                    label: 'Facilities', icon: 'pi pi-fw pi-bookmark',
                                    items: [
                                        { label: 'Cafeteria and Annapurna', icon: 'pi pi-fw pi-bookmark', to: '/app/facilitiesCafe' },
                                        { label: 'Library', icon: 'pi pi-fw pi-bookmark', to: '/app/facilitiesLib' },
                                        { label: 'Classroom', icon: 'pi pi-fw pi-bookmark', to: '/app/facilitiesClass' },
                                        { label: 'Hostel', icon: 'pi pi-fw pi-bookmark', to: '/app/facilitiesHostel' },
                                    ]
                                },
                            ]
                        },
                    ]
                },
                {
                    label: 'About', icon: 'pi pi-fw pi-bookmark',
                    items: [
                        {
                            label: 'Technical Staff', icon: 'pi pi-fw pi-bookmark',
                            items: [
                                { label: 'Consultant', icon: 'pi pi-fw pi-bookmark', to: '/app/consultant' },
                                { label: 'LRC', icon: 'pi pi-fw pi-bookmark', to: '/app/lrc' },
                                { label: 'Lab Technician', icon: 'pi pi-fw pi-bookmark', to: '/app/technician' },
                            ]
                        },
                        {
                            label: 'General Administration Staff', icon: 'pi pi-fw pi-bookmark',
                            items: [
                                { label: 'Account & Finance', icon: 'pi pi-fw pi-bookmark', to: '/app/account' },
                                { label: 'General Administration', icon: 'pi pi-fw pi-bookmark', to: '/app/general' },
                            ]
                        },
                    ]
                },
                {
                    label: 'Academics', icon: 'pi pi-fw pi-bookmark',
                    items: [
                        { label: 'Faculty', icon: 'pi pi-fw pi-bookmark', to: '/app/TechnicalStaff' },
                        { label: 'Academic Calendar', icon: 'pi pi-fw pi-bookmark', to: '/app/academicCal' },
                    ]
                },
                {
                    label: 'Admission', icon: 'pi pi-fw pi-bookmark',
                    items: [
                        {
                            label: 'Admission Procedure', icon: 'pi pi-fw pi-bookmark',
                            items: [
                                { label: 'B.Tech', icon: 'pi pi-fw pi-bookmark', to: '/app/btechPro' },
                                { label: 'Three Year', icon: 'pi pi-fw pi-bookmark', to: '/app/threeYearPro' },
                                { label: 'B. Tech. Lateral Entry', icon: 'pi pi-fw pi-bookmark', to: '/app/lateralPro' },
                                { label: 'PHD', icon: 'pi pi-fw pi-bookmark', to: '/app/phdPro' },
                            ]
                        },
                        {
                            label: 'Admission Help Desk', icon: 'pi pi-fw pi-bookmark',
                            items: [
                                { label: 'Admissions Related Inquiries', icon: 'pi pi-fw pi-bookmark', to: '/app/admissionInq' },
                                { label: 'Help-Desk Programs-wise', icon: 'pi pi-fw pi-bookmark', to: '/app/#' },
                            ]
                        },
                    ]
                },
                {
                    label: 'Training & Placement', icon: 'pi pi-fw pi-bookmark',
                    items: [
                        // { label: 'Training and Placement Cell', icon: 'pi pi-fw pi-bookmark',to:'/TrainingCell',},
                        {
                            label: 'Placement Records', icon: 'pi pi-fw pi-bookmark',
                            items: [
                                // { label: 'Student Placement Records', icon: 'pi pi-fw pi-bookmark' ,to: '/studentRecordChart'},
                                { label: 'Placement Highlights', icon: 'pi pi-fw pi-bookmark', to: '/app/StudentRecord' },
                            ]
                        },
                        { label: 'Contact Details', icon: 'pi pi-fw pi-bookmark', to: '/app/#', },
                    ]
                },
                {
                    label: 'For Student', icon: 'pi pi-fw pi-bookmark',
                    items: [
                        { label: 'Life on Campus', icon: 'pi pi-fw pi-bookmark', to: '/app/lifeAtCampus', },
                        {
                            label: 'Sports', icon: 'pi pi-fw pi-bookmark',
                            items: [
                                { label: 'Basketball', icon: 'pi pi-fw pi-bookmark', to: '/app/basketball' },
                                { label: 'Volleyball', icon: 'pi pi-fw pi-bookmark', to: '/app/vollyball' },
                                { label: 'Cricket', icon: 'pi pi-fw pi-bookmark', to: '/app/cricket' },
                                { label: 'Football', icon: 'pi pi-fw pi-bookmark', to: '/app/football' },
                            ]
                        },

                    ]
                },
                {
                    label: 'Careers', icon: 'pi pi-fw pi-bookmark',
                    items: [
                        { label: 'Careers Opening', icon: 'pi pi-fw pi-bookmark', to: '/app/careerOpen', },
                    ]
                },
                {
                    label: 'Alumni', icon: 'pi pi-fw pi-bookmark',
                    items: [
                        {
                            label: 'Alumni Photo Gallery', icon: 'pi pi-fw pi-bookmark',
                            items: [
                                { label: 'Alumni Meet', icon: 'pi pi-fw pi-bookmark', to: '/app/AlumniMeet' },
                                { label: 'Alumni Convocattion', icon: 'pi pi-fw pi-bookmark', to: '/app/AlumniConvo' },
                                { label: 'Alumni Carnival', icon: 'pi pi-fw pi-bookmark', to: '/app/AlumniCarnival' },
                            ]
                        },
                    ]
                },


            ]
        },
        // {
        //     label: 'UI Components', icon: 'pi pi-fw pi-sitemap',
        //     items: [
        //         { label: 'Form Layout', icon: 'pi pi-fw pi-id-card', to: '/formlayout' },
        //         { label: 'Input', icon: 'pi pi-fw pi-check-square', to: '/input' },
        //         { label: "Float Label", icon: "pi pi-fw pi-bookmark", to: "/floatlabel" },
        //         { label: "Invalid State", icon: "pi pi-fw pi-exclamation-circle", to: "invalidstate" },
        //         { label: 'Button', icon: 'pi pi-fw pi-mobile', to: '/button' },
        //         { label: 'Table', icon: 'pi pi-fw pi-table', to: '/table' },
        //         { label: 'List', icon: 'pi pi-fw pi-list', to: '/list' },
        //         { label: 'Tree', icon: 'pi pi-fw pi-share-alt', to: '/tree' },
        //         { label: 'Panel', icon: 'pi pi-fw pi-tablet', to: '/panel' },
        //         { label: 'Overlay', icon: 'pi pi-fw pi-clone', to: '/overlay' },
        //         { label: "Media", icon: "pi pi-fw pi-image", to: "/media" },
        //         { label: 'Menu', icon: 'pi pi-fw pi-bars', to: '/menu' },
        //         { label: 'Message', icon: 'pi pi-fw pi-comment', to: '/messages' },
        //         { label: 'File', icon: 'pi pi-fw pi-file', to: '/file' },
        //         { label: 'Chart', icon: 'pi pi-fw pi-chart-bar', to: '/chart' },
        //         { label: 'Misc', icon: 'pi pi-fw pi-circle-off', to: '/misc' },
        //     ]
        // },
        // {
        //     label: 'UI Blocks',
        //     items: [
        //         { label: 'Free Blocks', icon: 'pi pi-fw pi-eye', to: '/blocks', badge: "NEW" },
        //         { label: 'All Blocks', icon: 'pi pi-fw pi-globe', url: 'https://www.primefaces.org/primeblocks-react' }
        //     ]
        // },
        // {
        //     label: 'Icons',
        //     items: [
        //         { label: 'PrimeIcons', icon: 'pi pi-fw pi-prime', to: '/icons' }
        //     ]
        // },
        // {
        //     label: 'Pages', icon: 'pi pi-fw pi-clone',
        //     items: [
        //         { label: 'Crud', icon: 'pi pi-fw pi-user-edit', to: '/crud' },
        //         { label: 'Timeline', icon: 'pi pi-fw pi-calendar', to: '/timeline' },
        //         { label: 'Empty', icon: 'pi pi-fw pi-circle-off', to: '/empty' }
        //     ]
        // },
        // {
        //     label: 'Menu Hierarchy', icon: 'pi pi-fw pi-search',
        //     items: [
        //         {
        //             label: 'Submenu 1', icon: 'pi pi-fw pi-bookmark',
        //             items: [
        //                 {
        //                     label: 'Submenu 1.1', icon: 'pi pi-fw pi-bookmark',
        //                     items: [
        //                         { label: 'Submenu 1.1.1', icon: 'pi pi-fw pi-bookmark' },
        //                         { label: 'Submenu 1.1.2', icon: 'pi pi-fw pi-bookmark' },
        //                         { label: 'Submenu 1.1.3', icon: 'pi pi-fw pi-bookmark' },
        //                     ]
        //                 },
        //                 {
        //                     label: 'Submenu 1.2', icon: 'pi pi-fw pi-bookmark',
        //                     items: [
        //                         { label: 'Submenu 1.2.1', icon: 'pi pi-fw pi-bookmark' },
        //                         { label: 'Submenu 1.2.2', icon: 'pi pi-fw pi-bookmark' }
        //                     ]
        //                 },
        //             ]
        //         },
        //         {
        //             label: 'Submenu 2', icon: 'pi pi-fw pi-bookmark',
        //             items: [
        //                 {
        //                     label: 'Submenu 2.1', icon: 'pi pi-fw pi-bookmark',
        //                     items: [
        //                         { label: 'Submenu 2.1.1', icon: 'pi pi-fw pi-bookmark' },
        //                         { label: 'Submenu 2.1.2', icon: 'pi pi-fw pi-bookmark' },
        //                         { label: 'Submenu 2.1.3', icon: 'pi pi-fw pi-bookmark' },
        //                     ]
        //                 },
        //                 {
        //                     label: 'Submenu 2.2', icon: 'pi pi-fw pi-bookmark',
        //                     items: [
        //                         { label: 'Submenu 2.2.1', icon: 'pi pi-fw pi-bookmark' },
        //                         { label: 'Submenu 2.2.2', icon: 'pi pi-fw pi-bookmark' }
        //                     ]
        //                 }
        //             ]
        //         }
        //     ]
        // },

    ];

    const addClass = (element, className) => {
        if (element.classList)
            element.classList.add(className);
        else
            element.className += ' ' + className;
    }

    const removeClass = (element, className) => {
        if (element.classList)
            element.classList.remove(className);
        else
            element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
    }

    const wrapperClass = classNames('layout-wrapper', {
        'layout-overlay': layoutMode === 'overlay',
        'layout-static': layoutMode === 'static',
        'layout-static-sidebar-inactive': staticMenuInactive && layoutMode === 'static',
        'layout-overlay-sidebar-active': overlayMenuActive && layoutMode === 'overlay',
        'layout-mobile-sidebar-active': mobileMenuActive,
        'p-input-filled': inputStyle === 'filled',
        'p-ripple-disabled': ripple === false,
        'layout-theme-light': layoutColorMode === 'light'
    });

    return (
        <div className={wrapperClass} onClick={onWrapperClick}>
            <Tooltip ref={copyTooltipRef} target=".block-action-copy" position="bottom" content="Copied to clipboard" event="focus" />

            <AppTopbar onToggleMenuClick={onToggleMenuClick} layoutColorMode={layoutColorMode}
                mobileTopbarMenuActive={mobileTopbarMenuActive} onMobileTopbarMenuClick={onMobileTopbarMenuClick} onMobileSubTopbarMenuClick={onMobileSubTopbarMenuClick} />

            <div className="layout-sidebar" onClick={onSidebarClick}>
                <AppMenu model={menu} onMenuItemClick={onMenuItemClick} layoutColorMode={layoutColorMode} />
            </div>

            <div className="layout-main-container">
                <div className="layout-main">
               
                    <Switch  >
                    <Route path="/app/home" exact render={() => <Dashboard colorMode={layoutColorMode} location={location} />} />
                    <Route path="/app/TechnicalStaff" component={TechnicalStaff} />
                    <Route path="/app/UpcomingEvents" component={UpcomingEvents} />
                    <Route path="/app/Announcement" component={Announcement} />
                    <Route path="/app/AlumniMeet" component={AlumniMeet} />
                    <Route path="/app/AlumniConvo" component={AlumniConvo} />
                    <Route path="/app/AlumniCarnival" component={AlumniCarnival} />
                    <Route path="/app/StudentRecord" component={StudentRecord} />
                    <Route path="/app/TrainingCell" component={TrainingCell} />
                    <Route path="/app/highlightPage" component={HighlightPage} />
                    <Route path="/app/formlayout" component={FormLayoutDemo} />
                    <Route path="/app/input" component={InputDemo} />
                    <Route path="/app/floatlabel" component={FloatLabelDemo} />
                    <Route path="/app/invalidstate" component={InvalidStateDemo} />
                    <Route path="/app/button" component={ButtonDemo} />
                    <Route path="/app/table" component={TableDemo} />
                    <Route path="/app/list" component={ListDemo} />
                    <Route path="/app/tree" component={TreeDemo} />
                    <Route path="/app/panel" component={PanelDemo} />
                    <Route path="/app/overlay" component={OverlayDemo} />
                    <Route path="/app/media" component={MediaDemo} />
                    <Route path="/app/menu" component={MenuDemo} />
                    <Route path="/app/messages" component={MessagesDemo} />
                    <Route path="/app/blocks" component={BlocksDemo} />
                    <Route path="/app/icons" component={IconsDemo} />
                    <Route path="/app/file" component={FileDemo} />
                    <Route path="/app/chart" render={() => <ChartDemo colorMode={layoutColorMode} location={location} />} />
                    <Route path="/app/misc" component={MiscDemo} />
                    <Route path="/app/timeline" component={TimelineDemo} />
                    <Route path="/app/crud" component={Crud} />
                    <Route path="/app/empty" component={EmptyPage} />
                    <Route path="/app/documentation" component={Documentation} />
                    <Route path="/app/infrastructure" component={Infrastructure} />
                    <Route path="/app/campus" component={Campus} />
                    <Route path="/app/jaypeeYouthClub" component={JaypeeYouthClub} />
                    <Route path="/app/facilitiesCafe" component={FacilitiesCafe} />
                    <Route path="/app/facilitiesClass" component={FacilitiesClass} />
                    <Route path="/app/facilitiesHostel" component={FacilitiesHostel} />
                    <Route path="/app/facilitiesLib" component={FacilitiesLib} />
                    <Route path="/app/general" component={General} />
                    <Route path="/app/account" component={Account} />
                    <Route path="/app/consultant" component={Consultant} />
                    <Route path="/app/lrc" component={Lrc} />
                    <Route path="/app/technician" component={Technician} />
                    <Route path="/app/academicCal" component={AcademicCal} />
                    <Route path="/app/careerOpen" component={CareerOpen} />
                    <Route path="/app/btechPro" component={BtechPro} />
                    <Route path="/app/lateralPro" component={LateralPro} />
                    <Route path="/app/threeYearPro" component={ThreeYearPro} />
                    <Route path="/app/phdPro" component={PhdPro} />
                    <Route path="/app/admissionInq" component={AdmissionInq} />
                    <Route path="/app/studentRecordChart" component={StudentRecordChart} />
                    <Route path="/app/lifeAtCampus" component={LifeAtCampus} />
                    <Route path="/app/cricket" component={Cricket} />
                    <Route path="/app/football" component={Football} />
                    <Route path="/app/vollyball" component={Vollyball} />
                    <Route path="/app/basketball" component={Basketball} />
                    <Route path="/app/users" component={Users} />
                    </Switch>
                 
                </div>

                <AppFooter layoutColorMode={layoutColorMode} />
            </div>

            <AppConfig rippleEffect={ripple} onRippleEffect={onRipple} inputStyle={inputStyle} onInputStyleChange={onInputStyleChange}
                layoutMode={layoutMode} onLayoutModeChange={onLayoutModeChange} layoutColorMode={layoutColorMode} onColorModeChange={onColorModeChange} />

            <CSSTransition classNames="layout-mask" timeout={{ enter: 200, exit: 200 }} in={mobileMenuActive} unmountOnExit>
                <div className="layout-mask p-component-overlay"></div>
            </CSSTransition>

        </div>
    );

}

export default App;
