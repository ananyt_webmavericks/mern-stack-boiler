import React  from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';

export const AppTopbar = (props) => {

    return (
        <div className="layout-topbar">
            <Link to="/users" className="layout-topbar-logo">
                {/* <img src={props.layoutColorMode === 'light' ? 'assets/layout/images/header2.png' : 'assets/layout/images/header2.png'} alt="logo" width={2000}/> */}
                <span>Jaypee Admin Panel</span>
            </Link>

            <button type="button" className="p-link  layout-menu-button layout-topbar-button" onClick={props.onToggleMenuClick}>
                <i className="pi pi-bars"/>
            </button>

            <button type="button" className="p-link layout-topbar-menu-button layout-topbar-button" onClick={props.onMobileTopbarMenuClick}>
                <i className="pi pi-ellipsis-v" />
            </button>

                <ul className={classNames("layout-topbar-menu lg:flex origin-top", {'layout-topbar-menu-mobile-active': props.mobileTopbarMenuActive })}>
                    <li>
                        <Link to='/login'>
                        <button className="p-link layout-topbar-button">
                            <i className="pi pi-sign-out"/>
                            <span>sign out</span>
                        </button>
                        </Link>
                    </li>
                    
                    {/* <li>
                        <Link to='/app/users'>
                        <button className="p-link layout-topbar-button" onClick={props.onMobileSubTopbarMenuClick}>
                            <i className="pi pi-user"/>
                            <span>Profile</span>
                        </button>
                        </Link>
                    </li> */}
                </ul>
        </div>
    );
}
