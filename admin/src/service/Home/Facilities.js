import axios from 'axios';

export class Facilities {

    getFacilitiesCafe() {
        return axios.get('assets/demo/data/Home/FacilitiesCafe.json').then(res => res.data.data);
    }
    getFacilitiesHostel() {
        return axios.get('assets/demo/data/Home/FacilitiesHostel.json').then(res => res.data.data);
    }
    getFacilitiesClass() {
        return axios.get('assets/demo/data/Home/FacilitiesClass.json').then(res => res.data.data);
    }
    getFacilitiesLib() {
        return axios.get('assets/demo/data/Home/FacilitiesLib.json').then(res => res.data.data);
    }
   
    

}