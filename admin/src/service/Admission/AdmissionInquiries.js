import axios from 'axios';

export class AdmissionInquiries {

    getAdmissionInq() {
        return axios.get('assets/demo/data/Admission/AdmissionHelpDesk/AdmissionInquiry.json').then(res => res.data.data);
    }
   
   

}