import axios from 'axios';

export class AdmissionPro {

    getAdmissionBtech() {
        return axios.get('assets/demo/data/Admission/AdmissionProcedure/Btech.json').then(res => res.data.data);
    }
    getAdmissionLateral() {
        return axios.get('assets/demo/data/Admission/AdmissionProcedure/BtechLateral.json').then(res => res.data.data);
    }
    getAdmissionPhd() {
        return axios.get('assets/demo/data/Admission/AdmissionProcedure/Phd.json').then(res => res.data.data);
    }
    getAdmissionThree() {
        return axios.get('assets/demo/data/Admission/AdmissionProcedure/ThreeYear.json').then(res => res.data.data);
    }
   

}