import axios from 'axios';

export class Student {

    getLifeAtJaypee() {
        return axios.get('assets/demo/data/Student/LifeAt.json').then(res => res.data.data);
    }
    getBasket() {
        return axios.get('assets/demo/data/Student/Basket.json').then(res => res.data.data);
    }
   
    getFootball() {
        return axios.get('assets/demo/data/Student/Football.json').then(res => res.data.data);
    }
   
    getCricket() {
        return axios.get('assets/demo/data/Student/Cricket.json').then(res => res.data.data);
    }
   
    getVollyball() {
        return axios.get('assets/demo/data/Student/Vollyball.json').then(res => res.data.data);
    }
   

}