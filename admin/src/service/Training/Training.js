import axios from 'axios';

export class Training {

    getPlacementImages() {
        return axios.get('assets/demo/data/Training/PlacementImg.json').then(res => res.data.data);
    }
    getPlacementCell() {
        return axios.get('assets/demo/data/Training/PlacementCell.json').then(res => res.data.data);
    }
    getStudentRecord() {
        return axios.get('assets/demo/data/Training/StudentRecord.json').then(res => res.data.data);
    }

  
  

}