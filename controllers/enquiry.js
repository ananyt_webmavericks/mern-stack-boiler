const enquiryService = require("../services/enquiry");
const constants = require("../config/constants.json");

const createEnquiry = async (req, res) => {
  try {
    const createObject = req.body;

    const createEnquiry = await enquiryService.createEnquiry(createObject);

    if (!createEnquiry.status) {
      return res.status(400).send(createEnquiry);
    }

    res.status(200).send({
      status: true,
      message: "Enqiry Submitted Successfully!",
    });
  } catch (error) {
    res.status(400).send({
      status: false,
      message: error.message,
    });
  }
};

const getAllAdmissionEnquiry = async (req, res) => {
  try {
    const data = {
      type: constants.ENQUIRY_TYPE.ADMISSION,
    };

    const getAllEnquiries = await enquiryService.getAllEnquiry(data);

    res.status(200).send({
      status: true,
      enquiries: getAllEnquiries,
    });
  } catch (error) {
    res.status(400).send({
      status: false,
      message: error.message,
    });
  }
};

const getAllGeneralEnquiry = async (req, res) => {
  try {
    const data = {
      type: constants.ENQUIRY_TYPE.GENERAL,
    };

    const getAllEnquiries = await enquiryService.getAllEnquiry(data);

    res.status(200).send({
      status: true,
      enquiries: getAllEnquiries,
    });
  } catch (error) {
    res.status(400).send({
      status: false,
      message: error.message,
    });
  }
};

const createEnquryInfo = async (req,res) => {
    try {

        const dataObject = req.body

        const createEnquiry = await enquiryService.createEnquryInfo(dataObject)

        res.status(200).send({
            status:true,
            enquiryInfo:createEnquiry
        })

    }catch(error) {
        res.status(400).send({
            status:false,
            message:error.message
        })
    }
}

const getAdmissionEnquiryInfo = async (req,res) => {
    try {

        const data = {
            type:constants.ENQUIRY_TYPE.ADMISSION
        }

        const getAdmissionEnquiryInfo = await enquiryService.getEnquiryInfo(data) ;

        res.status(200).send({
            status:true,
            enquiryInfo:getAdmissionEnquiryInfo
        })

    }catch(error) {
        res.status(400).send({
            status:false,
            message:error.message
        })
    }
}

const getGeneralEnquiryInfo = async (req,res) => {
    try {

        const data = {
            type:constants.ENQUIRY_TYPE.GENERAL
        }

        const getGeneralEnquiryInfo = await enquiryService.getEnquiryInfo(data) ;

        res.status(200).send({
            status:true,
            enquiryInfo:getGeneralEnquiryInfo
        })

    }catch(error) {
        res.status(400).send({
            status:false,
            message:error.message
        })
    }
}

const updateEnquiryInfo = async (req,res) => {
    try {

        const updateObject = req.body;

        const updateInfo = await enquiryService.updateEnquiryInfo(updateObject);

        if(!updateInfo.status) {
            return res.status(400).send(updateInfo)
        }

        res.status(200).send(updateInfo)



    }catch(error) {
        res.status(400).send({
            status:false,
            message:error.message
        })
    }
}

module.exports = {
  createEnquiry,
  getAllAdmissionEnquiry,
  getAllGeneralEnquiry,
  createEnquryInfo,
  getAdmissionEnquiryInfo,
  getGeneralEnquiryInfo,
  updateEnquiryInfo
};
