const announcementService = require("../services/announcement");
const constants = require("../config/constants.json");

const createAnnouncement = async (req, res) => {
  try {
    const createObject = req.body;

    const createAnnouncement = await announcementService.createAnnouncement(
      createObject
    );

    res.status(200).send({
      status: true,
      announcement: createAnnouncement,
    });
  } catch (error) {
    res.status(400).send({
      status: false,
      message: error.message,
    });
  }
};

const changeAnnouncementStatus = async (req, res) => {
  try {
    const status = req.body.status;
    const announcementId = req.body.announcementId;

    const changeAnnouncementStatus =
      await announcementService.changeAnnouncementStatus(
        status,
        announcementId
      );

    if (!changeAnnouncementStatus.status) {
      return res.status(200).send(changeAnnouncementStatus);
    }

    res.status(200).send({
      status: true,
      announcement: changeAnnouncementStatus,
    });
  } catch (error) {
    res.status(400).send({
      status: false,
      message: error.message,
    });
  }
};

const getAllAnnouncements = async (req, res) => {
  try {
    const data = {
      type: constants.ANNOUNCEMENT.ANNOUNCEMENT,
      status: constants.STATUS.ACTIVE,
    };

    const getAllAnnouncements = await announcementService.getAllAnnouncements(
      data
    );

    res.status(200).send({
      status: true,
      announcement: getAllAnnouncements,
    });
  } catch (error) {
    res.status(400).send({
      status: false,
      message: error.message,
    });
  }
};

const getAllEvents = async (req, res) => {
  try {
    const data = {
      type: constants.ANNOUNCEMENT.EVENT,
      status: constants.STATUS.ACTIVE,
    };

    const getAllAnnouncements = await announcementService.getAllAnnouncements(
      data
    );

    res.status(200).send({
      status: true,
      events: getAllAnnouncements,
    });
  } catch (error) {
    res.status(400).send({
      status: false,
      message: error.message,
    });
  }
};

const deleteAnnouncement = async (req, res) => {
  try {
    const announcementId = req.params["id"];

    const deleteAnnouncement = await announcementService.deleteAnnouncement(
      announcementId
    );

    if (!deleteAnnouncement.status) {
      return res.status(200).send(deleteAnnouncement);
    }
    res.status(200).send({
      status: true,
      deleteAnnouncement: deleteAnnouncement,
    });
  } catch (error) {
    res.status(400).send({
      status: false,
      message: error.message,
    });
  }
};

module.exports = {
  createAnnouncement,
  changeAnnouncementStatus,
  getAllAnnouncements,
  getAllEvents,
  deleteAnnouncement,
};
