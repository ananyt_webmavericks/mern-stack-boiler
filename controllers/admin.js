const adminService = require("../services/admin");

const createAdmin = async (req, res) => {
  try {
    const adminUser = await adminService.createAdmin(req.body);
    if (!adminUser.status) {
      res.status(200).send(adminUser);
    } else {
      res.status(201).send({
        status: true,
        user: adminUser,
      });
    }
  } catch (error) {
    res.status(400).send({
      status: false,
      error: error,
    });
  }
};

const login = async (req, res) => {
  try {
    const { email, password } = req.body;
    const loginObject = await adminService.login(email, password);
    if (!loginObject.status) {
      res.status(200).send(loginObject);
    } else {
      res.status(200).send({
        status: true,
        login: loginObject,
      });
    }
  } catch (error) {
    res.status(400).send({
      status: false,
      error: error,
    });
  }
};

const changePassword = async (req, res) => {
  try {
    const { oldPassword, newPassword } = req.body;
    const user = req.user;

    const changePassword = await adminService.changePassword(
      oldPassword,
      newPassword,
      user
    );

    console.log("changePassword",changePassword)

    if (!changePassword.status) {
      res.status(400).send(changePassword);
    } else {
      res.status(200).send({
        status: true,
        message: "password changed successfully!!",
      });
    }
  } catch (error) {
    res.status(400).send({
      status: false,
      message: error.message,
    });
  }
};

const changeAdminStatus = async (req,res) => {
  try {

    const { adminId , status } = req.body

    const statusChanged = await adminService.changeAdminStatus(adminId,status);

    if(!statusChanged.status) {
      return res.status(200).send(statusChanged)
    }
    res.status(200).send({
      status:true,
      message:"admin status updated successfully"
    })

  }catch(error) {
    res.status(400).send({
      status:false,
      message:error.message
    })
  }
}

const getAllAdmins = async (req,res) => {

  try {

    const getAllAdmins = await adminService.getAllAdmins();

    res.status(200).send({
      status:true,
      users:getAllAdmins
    })

  }catch(error){
    res.status(400).send({
      status:false,
      message:error.message
    })
  }

}

module.exports = {
  createAdmin,
  login,
  changePassword,
  changeAdminStatus,
  getAllAdmins
};
