const express = require("express");
const app = express();
const path = require("path");
const fs = require("fs");

//importing app routes files
const adminRoutes = require("./routes/admin");
const enquiryRoutes = require("./routes/enquiry");
const announcementRoutes = require("./routes/announcements");

//importing environment file
require("custom-env").env(process.env.NODE_ENV, "./env");
const port = process.env.APP_PORT || 8042;
const bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const mongoose = require("mongoose");
const mongoUri = process.env.MONGOURL;

// connecting mongodb
mongoose.connect(
  mongoUri,
  { useNewUrlParser: true, useUnifiedTopology: true },
  function (error) {
    if (error) {
      console.log("error occured on mongo connection - ", error);
      process.exit();
    }
    console.log("database connected!");
  }
);

// mounting routes
app.use("/api/v1/admin", adminRoutes);
app.use("/api/v1/enquiry", enquiryRoutes);
app.use("/api/v1/announcement", announcementRoutes);

const appPath = path.join(__dirname, "admin", "build");
// this will be called once on startup
if (fs.existsSync(appPath)) {
  // render vue app on index route
  app.use("/", express.static(appPath));
}

// launching app
app.listen(port);
console.log("Server is listening at " + port);

exports = module.exports = app;
