const constants = require("../config/constants.json");

const validateEnquiryForm = async (req, res, next) => {
  if (Object.keys(req.body).length < 1) {
    return res.status(400).send({
      status: false,
      message: "Invalid request",
    });
  }

  const { type, name, email, phoneNumber, enquiry, program } = req.body;

  if (
    !(
      type === constants.ENQUIRY_TYPE.ADMISSION ||
      type === constants.ENQUIRY_TYPE.GENERAL
    )
  ) {
    return res.status(400).send({
      status: false,
      message: "type of enquiry is invalid",
    });
  }

  const specialCharacters = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
  if (!name || specialCharacters.test(name)) {
    return res.status(400).send({
      status: false,
      message: "Name can be empty and can not have special characters",
    });
  }

  const emailRegEx =
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (!email || !emailRegEx.test(email)) {
    return res.status(400).send({
      status: false,
      message: "Email can not empty or email is not valid",
    });
  }

  if (type === constants.ENQUIRY_TYPE.ADMISSION) {
    if (!program) {
      return res.status(400).send({
        status: false,
        message: "In case of enquiry for admission , program is mandatory",
      });
    }
  }

  if (!phoneNumber) {
    return res.status(400).send({
      status: false,
      message: "phoneNumber is mandatory",
    });
  }

  if (!enquiry) {
    return res.status(400).send({
      status: false,
      message: "enquiry is mandatory",
    });
  }

  return next();
};

const validateEnquiryInfo = async (req, res, next) => {
  if (Object.keys(req.body).length < 1) {
    return res.status(400).send({
      status: false,
      message: "Invalid request",
    });
  }

  const { phone, email , type } = req.body;

  if(!phone || !email || !type) {
    return res.status(400).send({
        status:false,
        message:"phone/email/type can not be empty"
    })
  }
  if(!(type === constants.ENQUIRY_TYPE.ADMISSION || type === constants.ENQUIRY_TYPE.GENERAL)) {
    return res.status(400).send({
        status:false,
        message:"type can only be admissions or general"
    })
  }

  return next();
};

module.exports = {
  validateEnquiryForm,
  validateEnquiryInfo,
};
