const constants = require("../config/constants.json");

const createAdmin = async (req, res, next) => {
  if (Object.keys(req.body).length < 1) {
    return res.status(400).send({
      status: false,
      message: "Invalid request!",
    });
  }

  const { name, email } = req.body;

  if (!name) {
    return res.status(400).send({
      status: false,
      message: "Name can not be empty",
    });
  }

  const specialCharacters = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
  if (specialCharacters.test(name)) {
    return res.status(400).send({
      status: false,
      message: "Name can not have special characters",
    });
  }

  if (!email) {
    return res.status(400).send({
      status: false,
      message: "Email can not be empty",
    });
  }

  const emailRegEx =
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (!emailRegEx.test(email)) {
    return res.status(400).send({
      status: false,
      message: "Email is invalid",
    });
  }
  return next();
};

const validateLogin = (req, res, next) => {
  if (Object.keys(req.body).length < 1) {
    return res.status(400).send({
      status: false,
      message: "invalid request!",
    });
  }

  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(400).send({
      status: false,
      message: "email/password can't be empty!",
    });
  }
  return next();
};

const validateChangePassword = (req, res, next) => {
  if (Object.keys(req.body).length < 1) {
    return res.status(400).send({
      status: false,
      message: "Invalid request!",
    });
  }
  const { oldPassword, newPassword } = req.body;

  if (!oldPassword || !newPassword) {
    return res.status(400).send({
      status: false,
      message: "oldPassword and newPassword can not be empty!",
    });
  }
  return next();
};

const validateChangeAdminStatus = async (req, res, next) => {
  const { adminId, status } = req.body;

  if (!adminId) {
    return res.status(400).send({
      status: false,
      message: "adminId and/or status can not be empty",
    });
  }

  if (
    !(
      constants.STATUS.ACTIVE === status || constants.STATUS.INACTIVE === status
    )
  ) {
    return res.status(400).send({
      status: false,
      message: "status field can only have active or inactive values",
    });
  }
  return next();
};

module.exports = {
  createAdmin,
  validateLogin,
  validateChangePassword,
  validateChangeAdminStatus,
};
