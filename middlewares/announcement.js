const constants = require("../config/constants.json");

const validateCreateAnnouncement = async (req, res, next) => {
  if (Object.keys(req.body).length < 1) {
    return res.status(400).send({
      status: false,
      message: "Invalid request",
    });
  }

  const { title, status, type } = req.body;

  if (!title || !type) {
    return res.status(400).send({
      status: false,
      message: "title/type can not be empty",
    });
  }

  if (
    !(
      type === constants.ANNOUNCEMENT.EVENT ||
      constants.ANNOUNCEMENT.ANNOUNCEMENT
    )
  ) {
    return res.status(400).send({
      status: false,
      message: "announcement type is invalid",
    });
  }

  if (
    !(
      status === constants.STATUS.ACTIVE || status === constants.STATUS.INACTIVE
    )
  ) {
    return res.status(400).send({
      status: false,
      message: "status is invalid",
    });
  }

  return next();
};

const validateChangeAnnouncementStatus = async (req, res, next) => {
  if (Object.keys(req.body).length < 1) {
    return res.status(400).send({
      status: false,
      message: "Invalid request",
    });
  }

  const { status, announcementId } = req.body;

  if (!announcementId) {
    return res.status(400).send({
      status: false,
      message: "status/announcementId can not be empty",
    });
  }

  if (
    !(
      status === constants.STATUS.INACTIVE || status === constants.STATUS.ACTIVE
    )
  ) {
    return res.status(400).send({
      status: false,
      message: "status value is invalid",
    });
  }
  return next();
};

module.exports = {
  validateCreateAnnouncement,
  validateChangeAnnouncementStatus,
};
