const jwt = require("jsonwebtoken");
const moment = require("moment");

const validateAdmin = async (req, res, next) => {
  const token = req.headers["authorization"];
  if (!token) {
    return res.status(401).send({
      status: false,
      message: "Unauthorized Access!",
    });
  }
  let bearer = token.split(" ");
  bearer = bearer[1];

  try {
    const decoded = jwt.verify(bearer, process.env.TOKEN_KEY);
    const now = moment().unix();
    if (now > decoded.exp) {
      return res.status(401).send({
        status: false,
        message: "Unauthorized Access!",
      });
    }
    req.user = decoded;
    return next();
  } catch (error) {
    return res.status(400).send({
      status: false,
      message: error,
    });
  }
};

module.exports = {
  validateAdmin,
};
